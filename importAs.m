function handle = importAs(package,depth)
    % Description:  Create aliased namespaces for packages. This
    %               generates a nested tree of function and subpackage
    %               handles that can be used in place of the the full
    %               package call. Note that this does not prevent use of
    %               the full call.
    % Inputs:
    %   package:    String of (sub)package from which to retrieve handles.
    %   depth:      (Optional) Number of levels of subpackages to parse.
    %               Set to inf (default) for full recursion, 0 to no
    %               recursion, or a positive integer value for a specified
    %               number of levels. For very large structures of nested
    %               packages, it may be desirable to select a shallow depth
    %               for efficiency reasons.
    % Outputs:
    %   handle:     (Nested) structure of function handles matching the
    %               same structure as the package directory and with the
    %               same calling syntax.
    % Example:
    % * Example 1:
    %   For a package with the following directory structure
    %       +mypack
    %           func1
    %           func2
    %           +mysubpack
    %               func3
    %   The normal (full) syntax to call 'func3' is:
    %       >> mypack.mysubpack.func3(args);
    %   Using 'importAs':
    %       >> local = importAs('mypack.mysubpack');
    %       >> local.func3(args);
    % * Example 2: Accessing subpackages.
    %   >> local = importAs('mypack');
    %   >> local.mysubpack.func3(args);
    % * Example 3: Shallow package mapping.
    %   >> local = importAs('myPack',0);
    %   >> local.mysubpack.func3(args);% -> Error! 'mysubpack' not present.
    %
    % Notes:
    % * This was inspired by 'module' on the Matlab File Exchange:
    %   https://nl.mathworks.com/matlabcentral/fileexchange/38014-module-encapsulate-matlab-package-into-a-name-space-module
    %   This was also featured in this article as a 'pick of the week':
    %   https://blogs.mathworks.com/pick/2014/07/18/simple-namespaces-and-brilliant-function-handles/
    %   However, as I understand it, 'module' has the opposite purpose of
    %   what I wanted: it limits the scope of packages. Rather, I wanted to 
    %   be able to access a package located anywhere on my path, give it a 
    %   custom namespace, and still retain access to all of its subpackages.
    % * Non-package directories within the package tree (i.e., directories
    %   not prefixed by '+') are ignored by default. This means such 
    %   directories can be used to conceal private scripts. This is the 
    %   usual behaviour of packages in Matlab.
    % * To do:
    %   - Add handling of classes and other file types
    % History:
    % * 2020/09/01: Created, A.Berry.
    
    % Default settings
    if ~exist('depth','var') || isempty(depth)
        depth   = Inf;
    end

    pkgPath         = strrep(package,'.',filesep);
    contents        = what(pkgPath);
    [~,pkgDir,~]    = fileparts(contents.path);
    
    if (pkgDir(1) == '+')
        handle      = struct();% Structure to hold function handles
        for file = [contents.m; contents.p]'
            [~,func,~]       = fileparts(file{:});
            handle.(func)    = str2func(sprintf('%s.%s',package,func));
        end

        % Recurse through subpackages, adding layers of nested function handles
        if depth > 0
            for subPkg = contents.packages'
                handle.(subPkg{:})  = importAs(sprintf('%s.%s',package,subPkg{:}),depth-1);
            end
        end
    end
end