function significance_bracket(axs,x1,x2,Y,p,varargin)
% Add single bracket in x1 and x2 position at Y height
% Accepts an optional input of the p value. If not provided the bracket
% appends a *.
% Modifications:
% * 2018/02/27, AB: Changed 'p' to an expected input, added linespec
%   optional inputs for changing colours.


    % Optional inputs
    if any(strcmpi(varargin, 'LineColor'))
        lineColor   = varargin{find(strcmpi(varargin,'LineColor'),1,'last') + 1};
    else
        lineColor   = [0,0,0];  % Black
    end
    if any(strcmpi(varargin, 'TextColor'))
        textColor   = varargin{find(strcmpi(varargin,'TextColor'),1,'last') + 1};
    else
        textColor   = [0,0,0];  % Black
    end
    
    % draw horizontal line
    plot(axs,[x1 x2],[Y Y],'Color',lineColor,'LineWidth',1);
    
    % Unitary dimensions
    bracketHeight = 0.2;
    star_y        = Y + 0.5;
   
    % Plot vertical lines
    plot(axs,[x1 x1], [Y-bracketHeight Y],'Color',lineColor,'LineWidth',1);
    plot(axs,[x2 x2], [Y-bracketHeight Y],'Color',lineColor,'LineWidth',1);
    
    % Plot star or p value
    if exist('p','var') && ~isempty(p)
        txt     = text(axs,0.5*(x1+x2),star_y,sprintf('p = %.3f',p),...
                        'Color',textColor,...
                        'HorizontalAlignment','center',...
                        'Interpreter','tex');
    else
        star = plot(axs,0.5*(x1+x2),star_y,...
                'Marker','*',...
                'MarkerSize',5,...
                'MarkerEdgeColor',textColor);
    end
    

end
