function stats = test_significance(data,varargin)
% Test siginificance over all data
% Step 1: Using Friedman non-parametric test
% If Step 1 p < 0.05 -> Step 2: Use post-hoc Wilcoxon pairwise
% non-parametric test.
% Input data should have the conditions per column.
%
% Usage:  
%        >> test_significance(Data); %Simple usage
%        >> test_significance(Data,Property,PropertyValue); 
%        where 
%        Data = [Column_Data1 Column_Data2 ... Column_Datan];
%        Property = 'alpha' or 'cols'
%                   'alpha' -> significance value to reject the null
%                   hypotesis.by default alpha= 0.05
%                   'cols' -> row vector containing a user predefined set
%                   of columns. All columns are tested by default
%       
% Example:
%       >> test_significance(Data,'alpha',0.05,[2:5]); Test significance
%       for datasets from columns 2 to 5 in Data with a significance
%       treshold of alpha = 0.05.
%
% Modifications;
% 2018/02/20: Previously did not generate pair table if not significant.
% Now does, to avoid issues when plotting arbitrary data sets.
          
        
    % Parse inputs
    alpha = 0.05; % Default significance
    cols  = (1:size(data,2));
    if ~isempty(varargin)
        if mod(length(varargin),2) == 0
            for i = 1 : length(varargin)/2
                switch varargin{2*i-1}
                    case 'alpha'
                        alpha = varargin{i*2};
                    case 'cols'
                        if ~any(varargin{i*2} <= cols(end))
                            error('Selected columns exceed Data dimension');
                        end
                        
                        cols  = varargin{i*2};
                end
            end
        else
            error('Input parameters in pairs');
        end
    end
    
    
    % Step 1
    % Check for significant differences across all conditions

    [fp,~,fstats] = friedman(data(:,cols),1,'off');
    stats.p = fp;
    sig_bracket = {};
    stats.pair = {};
    
    if fp > alpha
        stats.group_sig = 'Not significant';
    else
        stats.group_sig = 'Significant. Post-hoc Wilcoxon Signed Rank applied';
%     %     n_cond = length(cols);
%         for i = 1 : length(cols)
%             for j = i + 1 : length(cols)
%     %             fprintf('%d,%d\n',j,i);
%                 [wp,sig,w_stats] = signrank(Data(:,cols(i)),Data(:,cols(j)),'method','approximate');
%                 stats.pair = [stats.pair; {wp, [cols(i),cols(j)], sig}];
%                 if sig == 1
%                     sig_bracket = [sig_bracket, {[cols(i),cols(j)]}];
%                 else
%                     sig_bracket = [sig_bracket, {{}}];
%                 end
%             end
%         end
%         stats.pair = cell2table(stats.pair,'VariableNames',{'p','pair','sig'});
    end
    n_cond = length(cols);
    for i = 1 : length(cols)
        for j = i + 1 : length(cols)
%             fprintf('%d,%d\n',j,i);
            [wp,sig,w_stats] = signrank(data(:,cols(i)),data(:,cols(j)),'method','approximate');
            stats.pair = [stats.pair; {wp, [cols(i),cols(j)], sig}];
            if sig == 1
                sig_bracket = [sig_bracket, {[cols(i),cols(j)]}];
            else
                sig_bracket = [sig_bracket, {{}}];
            end
        end
    end
    stats.pair = cell2table(stats.pair,'VariableNames',{'p','pair','sig'});
   
end
