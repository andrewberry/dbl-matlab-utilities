% Description:  Example showing how to compute and display significant
%               differences between data.
% Note:
%   * test_significance.m assumes the data is non-parametric. The Friedman
%       test is used to detect the presence of significant differences,
%       then the Wilcoxon signed-rank test is used for pairwise
%       comparisons. Modifications may be required to first check for data
%       normality.
%
% History:
%   * 2019/08/15 AB: Created.

clear
close all

%% Generate some data for testing
nConditions     = 5;
nSubjects       = 11;
% nSubjects       = 101;

data            = randn(nSubjects,nConditions); % Random numbers (normally distributed)
data            = data + randperm(nConditions); % Add a difference between conditions

conditions      = char('A'-1+(1:nConditions)'); % Names of your conditions. As an example, they are just the letters A-E.

%% Check for significance
sigData         = test_significance(data);

% Display the result
%   * The "pair" column of the table shows the two columns of the input 
%   data being compared.
fprintf('p=%0.1e. %s\n',sigData.p,sigData.group_sig);
disp(sigData.pair);

%% Make a beautiful plot
figure('Name','Amazing results!');

% Create a normal boxplot
hPlot   = boxplot(data);
grid on;
xlabel('Condition');
ylabel('Performance (units)');

% Change the condition names
hAx(1)              = gca;  % Get the current axis handle, containing all plot properties.
hAx(1).XTickLabel   = conditions;   % Overwrite standard numeric ticks with nice text labels of your choosing.

% Add significance brackets to the existing plot
% * This is overlayed as an invisible axis
xData   = 1:nConditions;    % Just the X location of each condition in the plot
hAx(2)  = add_sig_brackets(hAx(1), sigData.pair, xData);