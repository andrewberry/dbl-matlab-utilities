function [br_axs,flag] = add_sig_brackets(axs, pairs, x_pos, varargin)
    % Description:  Add significant brackets on top of an existing axis.
    %               Creates a new axis on top of the current acs and 
    %               appends there the brackets.
    % Inputs: 
    %         axs:  Handle to the axis to which the brackets will be placed
    %       pairs:  Table containing the significant pairs and its p
    %               values. Entries with 'sig' equal to false will be
    %               removed.
    %                    pair     p     sig 
    %                   ______    _    _____
    % 
    %                   1    2    1    true 
    %                   3    4    2    true
    %       x_pos:  Optional vector contianing the x positions of the
    %               compared data, e.g.:
    %                   [0.5, 1.5, 2.5, 3.5, 5.5];
    %               If empty, will be generated based on the number of
    %               conditions found in the pairs table.
    %    varargin:  Optional property-value input pairs.
    %           Property    Description
    %           --------    -----------
    %           'Mode'      {'expand' <default, 'contract'}. Select whether
    %                       adding the brackets causes the figure to grow
    %                       ('expand') or whether the contents should
    %                       shrink ('contract').
    %           'LineColor' <RGB 1x3 vector or nx3 matrix>. Colour of the
    %                       significance brackets. To colour the brackets
    %                       independently, the input should be a
    %                       <n_pairs x 3> matrix with each row
    %                       corresponding to one of the significant pairs.
    %           'TextColor' <RGB 1x3 vector or nx3 matrix>. Same as
    %                       'LineColor', but for the colour of the text.
    % Outputs:
    %      br_axs:  Axis handle for significance brackets.
    %        flag:  Indicator of whether signficiance brackets were
    %               generated. Can be replaced by 'isempty(br_axs)'.
    %
    % Modifications:
    % * 2018/03/13, AB: 
    %   + Changed x_pos optional input into labelled input to simplify 
    %       filtering of additional property-value input argument pairs.
    %   + Added br_axs as an output variable.
    %   + Added option of changing bracket and text colours.
    %   + Added option of keeping behaviour of original implementation
    %       (figure size fixed, axes rescale), in addition to new
    %       implementation (figure size rescales, axes fixed).
    
    % Default axis handle
    br_axs = [];
    
    % Check for non empty brackets
    if isempty(pairs)
        flag    = false;
        return
    end    
    
    % Extract conditions
    conds = unique(reshape(pairs.pair,[],1));
    if ~exist('x_pos','var') || isempty(x_pos) % x_pos by default
        x_pos   = 1:length(conds);
    else
        if length(x_pos) ~= length(conds)
            error('Position vector must be same size as number of conditions');
        end
    end
    
    pairs.sig   = logical(pairs.sig);
    
    brkt_list   = pairs.pair(pairs.sig,:);
    p_values    = pairs.p(pairs.sig,:);
    n_brkt      = sum(pairs.sig);
    
    if n_brkt == 0
        flag = false;
        return
    end
    
    % Parse optional property-value inputs
    if any(strcmpi(varargin, 'Mode'))
        mode    = varargin{find(strcmpi(varargin,'Mode'),1,'last') + 1};
    else
        mode    = 'expand';
%         mode    = 'contract';
    end
    if any(strcmpi(varargin, 'LineColor'))
        lineColor   = varargin{find(strcmpi(varargin,'LineColor'),1,'last') + 1};
        if size(lineColor,1) == 1
            lineColor   = repmat(lineColor, n_brkt, 1);
        end
    else
        lineColor   = repmat([0,0,0], n_brkt, 1);   % Black
    end
    if any(strcmpi(varargin, 'TextColor'))
        textColor   = varargin{find(strcmpi(varargin,'TextColor'),1,'last') + 1};
        if size(textColor,1) == 1
            textColor   = repmat(lineColor, n_brkt, 1);
        end
    else
        textColor   = repmat([0,0,0], n_brkt, 1);   % Black
    end
    
    
    % Set predefined bracket height 
    brkt_h      = 0.6; %in cm
%     brkt_h      = 12; %in pixels
    
    fig             = axs.Parent;
%     fig.Resize      = 'off';
%     fig_tempUnits   = fig.Units;
    fig.Units       = 'centimeters';
    
    % Get sub axes if any
    axs.Tag     = 'Active'; % Temporary
    all_axs     = findall(fig,'Type','Axes');
    main_axs    = all_axs(~arrayfun(@(x) strcmp(axs.Tag,x.Tag),all_axs));
    top_axs     = main_axs(arrayfun(@(x) x.Position(2) > axs.Position(2),main_axs));
    sel_axs     = top_axs(arrayfun(@(x) (x.Position(1) - axs.Position(1) <= axs.Position(3)) && (x.Position(1) + x.Position(3) - axs.Position(1) >= 0),top_axs));    
    axs.Tag     = '';
    
    % Fix Axis positions
    arrayfun(@(x) set(x,'Units','centimeters'),all_axs);
    arrayfun(@(x) set(x,'Position',x.Position),all_axs); %?
    
    % Create new axis for significant brackets
    temp_Units      = axs.Units;
    br_axs          = axes( 'Parent', fig, ...
                            'Visible', 'on', ...
                            'NextPlot', 'add');               
    % Send axis to back            
    uistack(br_axs,'down',10);
%     fig_axs = subplot(2,1,2,axs);
%     br_axs  = subplot(2,1,1);
    br_axs.YAxis.Visible = 'off';
    br_axs.XAxis.Visible = 'off';
    br_axs.Units        = 'centimeters';
    axs.Units           = 'centimeters';
    br_axs.XLim         = axs.XLim;
    br_axs.XTick        = sort(x_pos);
    br_axs.XGrid        = 'on';
    br_axs.Tag          = 'SigBrackets';
    axs.Box             = 'off';
    
    if strcmpi(mode,'contract')
        % Set height of bracket axis
        br_axs.Position    = axs.Position;
        br_axs.Position(2) = br_axs.Position(2) + br_axs.Position(4) - brkt_h*(n_brkt+1);
        br_axs.Position(4) = brkt_h*(n_brkt+1);
        axs.Position(4)    = br_axs.Position(2) - axs.Position(2);
    else % 'expand' <default>
%          % Get sub axes if any
%         axs.Tag     = 'Active'; % Temporary
%         all_axs     = findall(fig,'Type','Axes');
%         main_axs    = all_axs(~arrayfun(@(x) strcmp(axs.Tag,x.Tag),all_axs));
%         top_axs     = main_axs(arrayfun(@(x) x.Position(2) > axs.Position(2),main_axs));
%         sel_axs     = top_axs(arrayfun(@(x) (x.Position(1) - axs.Position(1) <= axs.Position(3)) && (x.Position(1) + x.Position(3) - axs.Position(1) >= 0),top_axs));    
%         axs.Tag     = '';
%     
%         % Fix Axis positions
%         arrayfun(@(x) set(x,'Units','centimeters'),all_axs);
%         arrayfun(@(x) set(x,'Position',x.Position),all_axs); %?
        
        % Set height of bracket axis
        br_axs.Position     = axs.Position;
        br_axs.Position(2)  = br_axs.Position(2) + br_axs.Position(4);% - brkt_h*(n_brkt+1);
        br_axs.Position(4)  = brkt_h*(n_brkt+1);

        % Modify parent figure to accomodate new axis preserving original axes size 
        for i = 1 : length(sel_axs)
            sel_axs(i).Position(2) = sel_axs(i).Position(2) + br_axs.Position(4);
            if fig.Position(4) < sel_axs(i).Position(2) + sel_axs(i).Position(4)
                fig.Position(4) = fig.Position(4) + br_axs.Position(4);
            end
        end

        if fig.Position(4) < axs.Position(2) + axs.Position(4) + br_axs.Position(4)
            fig.Position(4) = fig.Position(4) + br_axs.Position(4);
        end
    end
    
    br_axs.YLim = [0 n_brkt+1];
    
    %Add significance brackets
    hold(br_axs,'on');
    for sig_i = 1 : n_brkt
       
        % Find relative position of the condition in the
        % list
        x1 = x_pos(conds == brkt_list(sig_i,1));
        x2 = x_pos(conds == brkt_list(sig_i,2));
        p  = p_values(sig_i);

        % Add bracket
%         significance_bracket(br_axs,x1,x2,sig_i,p);
        significance_bracket(br_axs,x1,x2,sig_i,p,'LineColor',lineColor(sig_i,:),'TextColor',textColor(sig_i,:));
                
    end
    hold(br_axs,'off');
    % Reset axis units 
    axs.Units       = temp_Units;
    flag = true;
end
