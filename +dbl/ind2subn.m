function sub = ind2subn(sz, ind)
    % Description:  Convert from linear (singleton) indices to
    %               multidimensional array subscript (tuple) indices.
    %               The built-in function 'ind2sub' requires the number of
    %               outputs to be known in advance; this packages all
    %               outputs into a cell array.
    % Inputs:
    %   sz  :   Size of the multidimensional array. Type: double (nDims).
    %   ind :   Singletons index/indices to convert. Type: double (nInd).
    % Outputs:
    %   sub :   Resultant tuple indices. Type: double (nInd x nDims).
    %
    % Example:
    %   % Compute subscript indices
    %   >> data    = reshape(1:24, 4,3,2);% Values = linear indices
    %   >> inds    = [1,2,12,24];
    %   >> subs    = dbl.ind2subn(size(data), inds);
    %   >> ans =
    %           1     1     1
    %           2     1     1
    %           4     3     1
    %           4     3     2
    %   % Check that they are correct
    %   >> subsC    = num2cell(subs);
    %   >> arrayfun(@(x) data(subsC{x,:}), 1:size(subs,1))
    %     ans =
    %           1     2    12    24
    %
    % History:
    % * 2020/10/06: Created, A. Berry.
    
    local       = module();
    ndims       = numel(sz);
    subVecs     = local.getArg(1:ndims, @ind2sub, sz, ind);% Captures all specified args in a cell array
    sub         = reshape(cell2mat(subVecs), [], ndims);
end