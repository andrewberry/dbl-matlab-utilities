function indPairs = permuteIndices(sz, dim, varargin)
    % Description:  For the specified dimension, find all unique pairs of 
    %               indices. With the remaining dimensions, find all
    %               valid array subscript (tuple) indices.
    % Notes:
    % * For a dimension with size > 2, the number of pairs will be >= to
    %   the number of possible indices.
    % * There are two ways to find all combinations across a dimension
    %   1) By construction: Form all possible indices through permutation.
    %   2) By decomposition: Find all possible indices, then remove.
    % * Uses 'permuteVars.m'.
    % History:
    % * 2020/08/28: Created, A. Berry.
    % * 2021/02/26: This function is not used and may not have ever been
    %   tested. However, it is a generalization of 'permuteIndexPairs' that
    %   might be useful for future reference.

    opts.outputIndType = 'sub';
    for iOpt = fieldnames(opts)'
        if any(strcmpi(varargin, iOpt))
            opts.(iOpt{:})      = varargin{find(strcmpi(varargin,iOpt{:}),1,'first') + 1};
        end
    end
    
    
    local       = module();
    
    isSel       = false(1,numel(sz));
    isSel(dim)  = true;

    % All subscript index values of each dimension
    subVals     = arrayfun(@(x) 1:x, sz, 'UniformOutput',false);
    
    subPairsSel = nchoosek(subVals{isSel}, 2);% Indices corresponding to the selected dimension
    subPairsRem = local.permuteVars(subVals(~isSel));% All remaining indices
%     subPairs    = cell(size(subPairsSel,1)*size(subPairsRem,1),2);
    
    % Can alternatively find all valid subs, exclude the column 'dim', then
    % find all unique values
%     subsAll     = local.ind2subn(sz, 1:prod(sz));
%     subPairsRem = unique(subsAll(:,~isSel), 'stable','rows');
    
    repSel      = sortrows(repmat(subPairsSel, size(subPairsRem,1), 1));
    repRem      = repmat(subPairsRem, size(subPairsSel,1), 1);% Unsorted
    
    % For each combination of the remaining dimensions, splice these values
    % in
    
    subPairs    = cell(1,2);
    for iPair = 1:2
        subPairs{iPair}     = [repRem(:,1:(dim-1)), repSel(:,iPair), repRem(:,dim:end)];
    end
    
    % Optionally convert to linear (singleton) indices
    if any(strcmpi(opts.outputIndType,{'ind','linear','singleton'}))
        singPairs               = zeros(size(repSel,1),2);
        for iPair = 1:2
           subCols              = mat2cellCol(subPairs{iPair});
           singPairs(:,iPair)   = sub2ind(sz,subCols{:});
        end
        indPairs    = singPairs;
    else
        indPairs    = subPairs;
    end
end

%% Local functions
function columns = mat2cellCol(mat)
    columns     = arrayfun(@(col) mat(:,col), 1:size(mat,2), 'UniformOutput',false);
end