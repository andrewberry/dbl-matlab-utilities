function inds = findN(data,dim,varargin)
    % Description:  The 'find' function is a bit awkward if you want to
    %               apply it independently to rows or columns of a matrix.
    %               This is a wrapper that enables the dimension to be
    %               specified.
    % Inputs:
    %   data:       Nr x Nc matrix of data or logical variables.
    %   dim:        Dimension to search: 1 (across rows, default), 2
    %               (across columns).
    %   varargin:   The usual arguments to the 'find' function.
    % Outputs:
    %   inds:       A cell array of indices. Its shape depends on the 
    %               dimension specified by dim:
    %                   dim = 1: A row vector of length Nc
    %                   dim = 2: A column vector of length Nr
    %               If its elements are singleton values, it can be
    %               converted to a numeric array with 'cell2mat'.
    % Example:
    % * Example 1: Singleton output
    %   >> x    = [1,0,0,0; 0,0,1,0];
    %   >> inds = cell2mat(findN(x,2));
    %       -> [1; 3]
    % * Example 2: Variable size output
    %   >> x        = randn(5,3);
    %   >> indsAll  = findN(x>0,1);
    %   >> indsLast = findN(x>0,1,1,'last');
    % History:
    % * 2020/08/31: Created, A. Berry.
    
    [Nr,Nc]     = size(data);
    if dim == 1
        inds    = arrayfun(@(c) find(data(:,c),varargin{:}), (1:Nc), 'UniformOutput',false);
    elseif dim == 2
        inds    = arrayfun(@(r) find(data(r,:),varargin{:}), (1:Nr)', 'UniformOutput',false);
    else
        error('Only 2-D indexing supported.');
    end
end