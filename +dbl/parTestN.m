function [results,opts] = parTestN(data,depVars,imVars,rmVars,varargin)
    % Description:  Perform a parametric analysis of variance (ANOVA) test  
    %               for >=3 groups for general combinations of independent-
    %               measures and repeated-measures independent variables.
    %               Tests of normality and homogeneity/sphericity of 
    %               variance are performed. Effect size is also computed.
    %               Data must be input as a table object with separate 
    %               columns for the dependent and independent variable(s).
    %               As an alternative to this function, see the built-in
    %               Matlab function 'multcompare' for N-way
    %               independent-measures analysis of parametric and
    %               non-parametric data.
    % Inputs:
    %   data    :   Table of stacked data, with separate columns for each
    %               of the dependent and independent variables. 
    %               Each table entry should be singleton (non-array) value.
    %               Dependent variables must have numeric values.
    %               Independent variables are assumed to be categorical
    %               (discrete and from a fixed set), and can be of type
    %               numeric (e.g., 'double'), text (e.g., 'char', 'string',
    %               'cellstr'), or 'categorical'.
    %   depVars :   String, char array, or cellstr array of one or more 
    %               table columns of dependent variables.
    %   imVars  :   String, char array, or cellstr array of zero or more 
    %               table columns of independent-measures independent
    %               variables.
    %   rmVars  :   String, char array, or cellstr array of zero or more 
    %               table columns of repeated-measures independent
    %               variables.
    %   varargin:   Various additional options input as either a struct or
    %               name-value pairs.
    %       alpha   :   Significance level (Fisher's criterion, the  
    %                   allowable probability of a Type I error). Used for 
    %                   the normality test, variance tests, and ANOVA.
    %                       double: 0.05 (default)
    %       display :   Print test results to command window
    %                       logical: true, false (default)
    %       normTest:   Test of normality across all independent variables.
    %                       string: 'kolmogorovsmirnov', 'lilliefors', 
    %                           'andersondarling', 'shapirowilk' (default)
    %       varTestIm:  Test of homogeneity of variance across the
    %                   independent-measures (between-subjects) variables.
    %                       string: 'Bartlett','LeveneQuadratic',
    %                           'LeveneAbsolute' (default),'BrownForsythe',
    %                           'OBrien'
    %       varTestRm:  Test of sphericity of variance across the
    %                   repeated-measures (within-subjects) variables.
    %                       string: 'Mauchly' (default}
    %       effectSize: Enables/disables computation of effect sizes.
    %                       logical: true (enabled, default), false
    %                       (disabled)
    %       sampleVar:  Name of internal sample index variable. This can be
    %                   changed if there is a naming conflict with the
    %                   input data.
    %                       string: 'SampleIndex' (default)
    %       sampleIndex: Name of table variable or array of categorical 
    %                   values indicating the sample index within each
    %                   combination of independent (independent- and
    %                   repeated-measures) factors, e.g., the subject
    %                   number. This is used to match samples across
    %                   repeated-measures factors (see 'rmDataTab').
    %                       Variable name: singleton string, char array, or
    %                                       cellstr.
    %                       Array data: 1D array of non-empty numeric,
    %                                   text, or categorical values.
    %                       Default: [] (empty)
    %       rmCatVar :  Name of internal variable for groups of
    %                   repeated-measures factors. This can be changed in
    %                   the event of a naming conflict with the input data.
    %                       string: 'RmGroup' (default)
    %       catToken :  String symbol or text used to indicate a
    %                   conctatenation of levels of independent variables.
    %                   Levels are concatenated to find all unique
    %                   combinations of independent factors.
    %                       string: ':' (default)
    %       actionIfEmptyRow :  Action to take if the input data contains 
    %                   empty elements in any column of a row.
    %                       string:
    %                           'error':    Display an error message if any 
    %                                       rows have an empty value. Stop
    %                                       execution.
    %                           'warning':  Display a warning if any rows
    %                                       have an empty value. Delete the
    %                                       row(s). 
    %                           'remove row' (default): Delete the row(s)
    %                                       without displaying a message.
    % Outputs:
    %   results:
    %   opts:       Structure containing all user-configurable function
    %               settings.
    % Notes:
    % * This function uses 'anova' and 'ranova' of the Statistics and
    %   Machine Learning Toolbox. These require the data to be input as a
    %   table object, in which each sample and independent-measures factor
    %   is in a separate row and each repeated-measures factor is in a 
    %   separate column. This function automatically converts vertical
    %   stack of data to horizontal stacking where necessary.
    % * 'unstack' is used to convert a single column of data into multiple
    %   columns representing (combinations of) levels of repeated-measures
    %   indepedent variables. However, if the grouping values are not
    %   specified correctly, the function will automatically try to
    %   aggregate values that have the same grouping levels. By default,
    %   the values are summed together and a warning is thrown if this
    %   occurs. To avoid this, this script estimates the number of
    %   measurements corresponding to each unique combination of these
    %   grouping levels; however, this can be overridden with the input
    %   'groupVars' and can lead to unexpected results if unused
    %   independent variables or levels are included.
    %   For example, if measurements of some quantity are made from 12 
    %   participants for each their left and right side but only the left
    %   side is of interest, remove the right-side data before running this
    %   script. Otherwise, if 'side' is not declared as a repeated-measures
    %   independent variable, it will be assumed to be additional
    %   independent measurements (in other situations, this may be the
    %   desired behaviour).
    %   Levels of variables can be ignored with the 'ignoreLevels' option.
    % To do:
    % * Add output strings or tables to summarize the important
    %   interpretations. E.g., validation of assumptions, sphericity
    %   corrections, significant effects, effect sizes.
    % * Effect size calculations not yet validated. Computation currently
    %   requires a sample index variable, which might not be necessary.
    % * Can use multcompare to do posthoc analysis using the generated
    %   model: 
    %   https://nl.mathworks.com/matlabcentral/answers/140799-3-way-repeated-measures-anova-pairwise-comparisons-using-multcompare
    % History:
    % * 2020/09/01: Created, A.Berry.
   
    % Load local namespace for accessing functions within the same package
    local                       = module();
    
    % Default optional name-value parameters
    defaults.display            = false;
    defaults.alpha              = 0.05;
    defaults.normTest           = 'Shapiro-Wilk';
    defaults.varTestIm          = 'LeveneAbsolute';% Test homogeneity of variance (important for indep samples). Method used in SPSS. Others: 'Bartlett' (default), 'LeveneQuadratic', 'LeveneAbsolute', 'BrownForsythe', 'OBrien'
    defaults.varTestRm          = 'Mauchly';
    defaults.effectSize         = true;
    defaults.sampleVar          = 'SampleIndex';
    defaults.sampleIndex        = [];
    defaults.rmCatVar           = 'RmGroup';
    defaults.catToken           = ':';% Symbol for concatenating categorical variables
    defaults.actionIfEmptyRow   = 'RemoveRow';

    %%% Parse user inputs
    % Overwrite the default options with any user-supplied name-value pairs
    % * Do not use the built-in error-checker, since its messages are not
    %   very clear or useful to the user. Error-checking of positional and 
    %   optional arguments is performed separately below.
    p                           = inputParser;
    p.FunctionName              = funcname();
    p                           = addParameters(p, defaults);
    p.parse(varargin{:});
    opts                        = p.Results;
    
%     issingleton = @(x) (numel(x) == 1);
%     istext      = @(x) ischar(x) || isstring(x) || iscellstr(x);
%     istabvar    = @(x) islogical(x) || isnumeric(x) || istext(x);
%     p.FunctionName              = funcname();% default: '' (empty)
%     p.addRequired( 'tabData',   @istable );
%     p.addRequired( 'depVars',   istabvar );
%     p.addRequired( 'imVars',    istabvar );
%     p.addRequired( 'rmVars',    istabvar );
%     p.addParameter('display',   defaults.display,   @(x) islogical(x) && issingleton(x) );
%     p.addParameter('alpha',     defaults.alpha,     @(x) isnumeric(x) && issingleton(x) );
%     p.addParameter('normTest',  defaults.normTest,  istext );
%     p.addParameter('varTestIm', defaults.varTestIm, istext );
%     p.addParameter('varTestRm', defaults.varTestRm, istext );
%     p.addParameter('effectSize',defaults.effectSize, @islogical );
%     p.addParameter('sampleVar', defaults.sampleVar, istext );
%     p.parse( tabData, depVars, imVars, rmVars, varargin{:} );
%     opts                        = p.Results;

    %%% Check and sanitize options arguments
    % Normality test
    opts_normTestName   = {'kolmogorovsmirnov', 'lilliefors', 'andersondarling', 'shapirowilk'};
    opts_normTestFunc   = {@kstest, @lillietest, @adtest, local.swtest};
    sel_normTest        = strcmpi(opts_normTestName, strrep(opts.normTest,'-',''));
    if sum(sel_normTest == 1)
        opts.normTestFunc   = opts_normTestFunc{sel_normTest};
    else
        error('Unrecognized option for normality test: %s.', opts.normTest)
    end

    % Homogeneity of variance test (independent-measures factors)
    opts_varTestImName  = {'Bartlett','LeveneQuadratic','LeveneAbsolute','BrownForsythe','OBrien'};
    sel_varTestIm       = strcmpi(opts_varTestImName, opts.varTestIm);
    if sum(sel_varTestIm) == 1
        % Confirm the selection and ensure correct capitalization.
        opts.varTestIm  = opts_varTestImName{sel_varTestIm};
    else
        error('Unrecognized option for homogeniety of variance test: %s.', opts.varTestIm);
    end

    % Sphericity of variance test (repeated-measures factors)
    opts_varTestRmName  = {'Mauchly'};
    opts_varTestRmFunc  = {@mauchly};
    sel_varTestRm       = strcmpi(opts_varTestRmName, opts.varTestRm);
    if sum(sel_varTestRm) == 1
        opts.varTestRmFunc  = opts_varTestRmFunc{sel_varTestRm};
    else
        error('Unrecognized option for sphericity of variance test: %s.', opts.varTestRm);
    end

    %%% Check the data inputs
    % dataTab
    if ~istable(data) || isempty(data) || all(ismissing(data),'all')
        error("Input '%s' must be a non-empty table object.", varname(data));
    end
    
    % Convert all dependent and independent variable indexing variables to
    % column names. This is the most stable indexing method if the table
    % changes shape.
    % * Basic error-checking: Existence of variable.
    % * Casts column names to 'cellstr'.
    depVarNames             = colIndexToName(data, depVars);
    imVarNames              = colIndexToName(data, imVars);
    rmVarNames              = colIndexToName(data, rmVars);
    indepVarNames           = [imVarNames, rmVarNames];
    
    % Check that at least one dependent and independent variable is
    % selected
    if isempty(depVarNames)
        error("At least one dependent variable ('depVars') must be supplied.");
    end
    if isempty(indepVarNames)
        error("At least one independent variable ('imVars' or 'rmVars') must be supplied.");
    end
    
    % Create a new table consisting of only the selected variables
    colNames                = data.Properties.VariableNames;
    [~, indVars]            = ismember([indepVarNames, depVarNames], colNames);
    dataTab                 = data(:,indVars);
    
    % Check for empty rows in the data
    % * None of the columns may be empty
    isEmptyRow              = any(ismissing(dataTab),2);
    if any(isEmptyRow)
        if all(isEmptyRow)
            error("Input '%s' contains only empty rows.", varname(data));
        end
        if contains(opts.actionIfEmptyRow, 'error', 'IgnoreCase', true)
            % Throw error
            error("Input '%s' contains %d empty rows.", varname(data), sum(isEmptyRow));
        elseif contains(opts.actionIfEmptyRow, 'warning', 'IgnoreCase', true)
            % Display warning and remove rows
            warning("Input '%s' contains %d empty rows. Rows removed: %s.", varname(data), sum(isEmptyRow), strjoin(compose('%d',find(isEmptyRow)),', '));
            dataTab(isEmptyRow,:)   = [];
        else
            % Remove rows without displaying warning
            dataTab(isEmptyRow,:)   = [];
        end
    end
    
    % Number of each variable type
    nRows                   = size(dataTab,1);
    nDepVars                = numel(depVarNames);
    nIndepVars              = numel(indepVarNames);
    nImVars                 = numel(imVarNames);
    nRmVars                 = numel(rmVarNames);
    
    % Logical indices for each variable
    % * Useful for operations combining conditions
    colNames                = dataTab.Properties.VariableNames;
    isDepVar                = ismember(colNames, depVarNames);
    isIndepVar              = ismember(colNames, indepVarNames);
    isImVar                 = ismember(colNames, imVarNames);
    isRmVar                 = ismember(colNames, rmVarNames);
    
    % Ordinal indices for each variable
    % * Useful when the order of values is important
    % * Note: 'find(logical)' computes ordinal indices but does not
    %   maintain the order of elements. Instead, use the secondary output
    %   of 'ismember'.
    [~, depVarInds]         = ismember(depVarNames, colNames);
    [~, indepVarInds]       = ismember(indepVarNames, colNames);
    [~, imVarInds]          = ismember(imVarNames, colNames);
    [~, rmVarInds]          = ismember(rmVarNames, colNames);
    
    % Check and convert variable type
    % * Dependent variables are assumed to be numeric data
    % * Independent variables are assumed to be categorical and are
    %   converted to 'cellstr' for consistent behaviour during string
    %   operations. Conversion to 'categorical' is performed first to
    %   ensure that numeric values are interpreted as strings. 
    %   Update: Converting to 'string' does the same.
    isDepVarNumeric     = varfun(@isnumeric, dataTab(:,isDepVar), 'OutputFormat', 'uniform');
    if ~all(isDepVarNumeric)
        error("Dependent variables must contain numeric data. Check variables: %s.", strjoin(depVarNames(isDepVarNumeric),', ') );
    end
    dataTab     = convertvars(dataTab, isIndepVar, 'string');

    % Sample index
    % * If one is supplied, check that it has the correct number of
    %   elements.
    % * If one is not supplied, check that 
    if isempty(opts.sampleIndex)
        % Not supplied: Automatically generate it later
        sampleIndex     = [];
    else
        % Supplied: Check whether it is a variable name or categorical
        % array
        istext  = @(x) isstring(x) || ischar(x) || iscellstr(x);
        if istext(opts.sampleIndex) && ( numel(string(opts.sampleIndex)) == 1 )
            if ismember(opts.sampleIndex, data.Properties.VariableNames)
                sampleIndex     = reshape( data.(opts.sampleIndex), [], 1);
            else
                error("'SampleIndex' variable name not found in input data.");
            end
        else
            sampleIndex     = reshape( opts.sampleIndex, [], 1);
        end
        if numel(sampleIndex) ~= size(data,1)
            error("'SampleIndex' data must have as many elements (rows) as the data.");
        end
        sampleIndex     = categorical( sampleIndex(~isEmptyRow) );
        isSampleEmpty   = ismissing(sampleIndex);
        if any(isSampleEmpty)
            error("'SampleIndex' data must not contain empty entries.");
        end
    end
    
    % Group the data by the combinations of independent variables
    % * The normality test, homogeneity of variance test (IM factors), and
    %   sphericity of variance test (RM factors) are all performed with
    %   different types of groupings of the data. These groupings are as
    %   follows:
    %       Normality:      Each combination of IM and RM factors assessed
    %                       individually.
    %       Homogeneity:    Each combination of RM factors assessed across
    %                       combinations of IM factors.
    %       Sphericity:     Globally assessed across each RM factor and
    %                       combinations of RM factors.
    % * If there are RM factors, the dependent variable must be split
    %   ('unstacked') into multiple columns to comply with the format of
    %   Matlab's repeated-measures model.
    
    % Group all unique combinations of levels of independent factors and
    % label sample indices within each group
    % * Each group can be thought of as a single box in a box plot.
    % * This grouping is used for performing normality tests, and the
    %   sample numbering helps later when dividing repeated-measures
    %   variables into separate columns. 
    % * Unique groups of independent factors are found by concatenating
    %   together the levels of each row's independent variables. The number
    %   of groups will be less than or equal to the product of the number
    %   of levels of each independent variable:
    %   >> prod( varfun(@(x) numel(unique(x)), dataTab(:,isIndepVar), 'OutputFormat', 'uniform') )
    % * Samples are numbered in order of occurrence in each group. If
    %   repeated-measures factors are analyzed, the numbering of the
    %   samples will affect the outcome. It is assumed that the data is
    %   input in the correct sequence (there is no way of automatically
    %   checking this). 
    % * The sample index is appended as a table variable. To avoid
    %   potential naming conflicts, the name can be set as an optional
    %   input.
    indepLevelsCat      = join(dataTab{:,isIndepVar}, opts.catToken, 2);
    [indepGroupNames, tabIndOfIndepGroup, indepGroupIndOfTab]   = unique(indepLevelsCat);
    nIndepGroups        = numel(indepGroupNames);
    indepGroupLevelsTab = dataTab(tabIndOfIndepGroup,isIndepVar);% nGroups x nIndepVars
%     groupSampleCount    = accumarray(indepGroupIndOfTab, 1);

    % Add a sample index if one is not supplied by the user
    if isempty(sampleIndex)
        sampleIndex         = zeros(nRows, 1);
        for iIndepGroup = 1:nIndepGroups
            isRowInGroup                = (indepGroupIndOfTab == iIndepGroup);
            nSamples                    = sum(isRowInGroup);
            sampleIndex(isRowInGroup)   = (1:nSamples)';
        end
    end
    dataTab.(opts.sampleVar)    = categorical(sampleIndex);
    
    % Update indexing variables to account for a change of size in dataTab
    colNames                = dataTab.Properties.VariableNames;
    isDepVar                = ismember(colNames, depVarNames);
    isIndepVar              = ismember(colNames, indepVarNames);
    isImVar                 = ismember(colNames, imVarNames);
    isRmVar                 = ismember(colNames, rmVarNames);
    
    %%% Perform tests
    
    results = [];
    for iDep = nDepVars:-1:1% Reverse order to pre-allocate memory
        % Perform normality tests
        % * Null hypothesis: Data comes from a standard normal distribution.
        % * Determines whether parametric or non-parametric statistical 
        %   comparisons should be used.
        % * Check normality for all combination of levels of independent
        %   variables found earlier.
        nTest   = [];
        for iIndepGroup = nIndepGroups:-1:1
            isRowInGroup        = (indepGroupIndOfTab == iIndepGroup);
            [~, nTest(iIndepGroup).pValue, nTest(iIndepGroup).tStat] = ...
                opts.normTestFunc(dataTab{isRowInGroup,depVarInds(iDep)}, opts.alpha);
        end
        normTab             = [indepGroupLevelsTab, struct2table(nTest)];
        normTab.isNormal    = (normTab.pValue > opts.alpha);

        % Construct a stastical model object and perform the ANOVA test
        % * Although methodologically the variance tests are usually done
        %   first, they actually require the output of the ANOVA function.
        % * Regardless of whether or not repeated-measures factors are
        %   present, a 'repeated-measures model' data object is
        %   constructed since it is capable of representing all
        %   combinations of IM and RM factors (independent-measures, 
        %   repeated-measures, and mixed). This object is also used to
        %   assess sphericity of variance.
        % * To use this model structure, the data must be split
        %   ('unstacked') into multiple columns, each representing a
        %   different combination of RM levels. If no RM factor is present,
        %   then the data remains a single column.
        if nRmVars > 0
            % Reshape the data to have a separate column for each combination 
            % of levels of the repeated-measures variables.
            % * Syntax: unstack( tableData, columnVariables, indexVariable, 'GroupingVariables', additionalVariables)
            
            % Group the data by combinations of RM levels
            rmLevelsCat         = join(dataTab{:,isRmVar}, opts.catToken, 2);
            [rmGroupNames, tabIndOfRmGroup, rmGroupIndOfTab]    = unique(rmLevelsCat);
            nRmGroups           = numel(rmGroupNames);
            rmGroupSampleCount  = accumarray(rmGroupIndOfTab, 1);
            rmGroupLevelsTab    = dataTab(tabIndOfRmGroup,isRmVar);% nGroups x nIndepVars
            if numel(unique(rmGroupSampleCount)) ~= 1
                error("An inconsistent number of samples was found between grouping of repeated-measures levels. Check that the input has an equal number of samples in each experimental condition.");
            end
            
            % Create a placeholder name for the groups of RM levels that
            % can also be used as a valid column variable name:
            % 'rmGroup<groupnumber>'
            % * Leading zeroes are added to <groupnumber> so that the
            %   sorting of the names is not different than the original
            %   concatenated name.
            nDigits                     = floor(nRmGroups/10)+1;
            rmPrintSpec                 = sprintf('rmGroup%%0%dd', nDigits);
            rmLevelsPlaceholder         = compose(rmPrintSpec, rmGroupIndOfTab);
            rmGroupLevelsTab.ColumnName = rmLevelsPlaceholder(tabIndOfRmGroup);
            
            % Create a new 'unstacked' table object
            % * The RM variables are replaced with a single variable
            %   consisting of the concatenated RM levels. The unique values
            %   of this become separate data columns.
            % * The new columns are named 'rmGroup<number>' because the
            %   concatenated levels might not be valid variable names. The
            %   number has an adjustable number of leading zeros to ensure
            %   that the names are sorted in the correct order.
            % * Contents of 'rmDataTab':
            %   Stacked:   [depVar, opts.sampleVar, opts.rmCatVar]
            %   Unstacked: [opts.sampleVar, <rmCatVar level 1>, ..., <rmCatVar level N>]
            rmDataTab                   = dataTab(:,~isRmVar);% RM vars removed
            rmDataTab.(opts.rmCatVar)   = rmLevelsPlaceholder;
            rmDataTab                   = unstack(rmDataTab, depVarNames{iDep}, opts.rmCatVar);
            rmColNames                  = rmGroupLevelsTab.ColumnName;% Placeholder names used in 'rmDataTab'
            
            % Check that no data was lost (can happen if the grouping
            % variables are not defined correctly)
            if size(rmDataTab,1)*numel(rmColNames) ~= size(dataTab,1)
                warning("Unstacking data resulted in a different number of samples. Check 'rmDataTab'. Remove unused measurements from the input data.");
            end

            % Create the statistical model object and perform the ANOVA
            % test
            % * Create a table associating the columns with the RM vars
            % * Define the within-subjects variables and model expression
            if nImVars > 0
                rmModExpr       = [strjoin(rmColNames,','),' ~ 1 + ',strjoin(imVarNames,'*')];% Wilkinson formula, e.g., 'rmA,rmB,rmC ~ inA*inB'
            else
                rmModExpr       = [strjoin(rmColNames,','),' ~ 1'];% Wilkinson formula, e.g., 'rmA,rmB,rmC ~ inA*inB'
            end
            withinVars          = rmGroupLevelsTab(:,1:(end-1));% Associate the repeated-measures levels with the columns
            withinExpr          = strjoin(rmVarNames,'*');% Output all main and interaction effects of the RM variables

            rmMod               = fitrm(rmDataTab, rmModExpr, 'WithinDesign', withinVars);
            [anovaTab,~,cMat,~] = ranova(rmMod, 'WithinModel', withinExpr);

        else% Independent-measures variables only
            % Create the regression model object and perform the ANOVA
            % * No reshaping of the data required
            % * No within-subjects design (between-subjects only)   
            rmDataTab           = dataTab;% Renaming for re-usability of code outside of if/else statement
            rmColNames          = depVarNames(iDep);
            rmModExpr           = [strjoin(rmColNames,','),' ~ 1 + ',strjoin(imVarNames,'*')];% Wilkinson formula, e.g., 'rmA,rmB,rmC ~ inA*inB'

            rmMod               = fitrm(rmDataTab, rmModExpr);
            anovaTab            = anova(rmMod);
        end
        anovaTab.isSig          = (anovaTab.pValue < opts.alpha);

        % Compute effect size
        if opts.effectSize
%             n                       = numel(unique(data.(opts.groupVars)));% Number of observations per condition
            n                       = [];
            effectTab               = local.computeEffect(anovaTab, n);
            effectTab.Properties.VariableNames     = strcat('effect_', effectTab.Properties.VariableNames);
            anovaTab                = [anovaTab, effectTab];
        end

        % Test for homogeneity of variance across independent-measures
        % variables
        % * Null hypothesis: The variances of the different populations are
        %   equal. Variances are not significantly different if p > alpha.
        % * Each combination of IM variables is considered a distinct
        %   group/population. They are compared for each combination of
        %   repeated-measures variables ('rmColNames' variable).
        if nImVars > 0
            % Find all permutations of levels of independent-measures variables
            % * Create categorical labels for each data row by combining
            %   the values in each IM column as strings.
            rmDataColNames      = rmDataTab.Properties.VariableNames;% Row vector
            isImVar             = ismember(rmDataColNames, imVarNames);
            imLevelsCat         = join(rmDataTab{:,isImVar}, opts.catToken, 2);
            [imGroupNames, tabIndOfImGroup, imGroupIndOfTab]    = unique(imLevelsCat);
            nImGroups           = numel(imGroupNames);
            imGroupLevelsTab    = rmDataTab(tabIndOfImGroup,isImVar);

            % For each combination of RM variables, perform the test across
            % the IM factors.
            varTestIm           = [];
            for iRmCol = length(rmColNames):-1:1
                if nRmVars > 0
                    varTestIm(iRmCol).rmGroupName       = rmColNames{iRmCol};
                    varTestIm(iRmCol).rmGroupFactors    = strjoin(rmGroupLevelsTab{iRmCol,1:(end-1)}, ' x ');
                end
                [varTestIm(iRmCol).pValue, temp]    = vartestn(rmDataTab.(rmColNames{iRmCol}), imGroupIndOfTab, 'TestType',opts.varTestIm, 'Display','off');
                varTestIm(iRmCol).F                 = temp.fstat;
                varTestIm(iRmCol).DF                = temp.df;
            end
            varTabIm                = struct2table(varTestIm);
            varTabIm.isHomogeneous  = (varTabIm.pValue > opts.alpha);
        else
            varTabIm                = [];
        end

        % Test for sphericity across repeated-measures variables
        % * Null hypothesis: The variances of differences between all pairs
        %   of repeated-measures conditions is similar to any other two
        %   pairs of conditions.
        % * Performed for all within-subjects factors.
        % * In the case of that a repeated-measures variable has only 2 
        %   levels, the test is redundant (guaranteed to pass), however the
        %   Matlab output will show it has failed. Screen this by checking
        %   if the test statistic is identically equal to 1.
        if ~isempty(rmVars)
            varTabRm                = opts.varTestRmFunc(rmMod, cMat);
            varTabRm.isSpherical    = ( (varTabRm.pValue > opts.alpha) | (varTabRm.W == 1) );
            
            % Append epsilon correction factors in case other calculations
            % are necessary.
            epsTab                  = epsilon(rmMod, cMat);
            epsTab.Properties.VariableNames     = strcat('epsilon_', epsTab.Properties.VariableNames);
            varTabRm                = [varTabRm, epsTab];

            % Append row names to make the results more readable.
            rowNames                        = anovaTab.Properties.RowNames;
            varTabRm.Properties.RowNames    = rowNames(contains(rowNames,'(Intercept)'));
        else
            varTabRm                = [];
        end
    
        %%% Optionally display the results
        if opts.display
            fprintf("Dependent variable: \t'%s'\n", depVar);
            fprintf("Normality test (%s): \t%s\n", opts.normTest, local.ifelse(all(normTab.isNormal),'Passed','Failed'));
            disp(normTab);
            if ~isempty(varTabIm)
                fprintf("Homogeneity of variance test (%s): \t%s\n", opts.varTestIm, local.ifelse(all(varTabIm.isHomogeneous),'Passed','Failed'));
                disp(varTabIm);
            end
            if ~isempty(varTabRm)
                fprintf("Sphericity of variance test (%s): \t%s\n", opts.varTestRm, local.ifelse(all(varTabRm.isSpherical),'Passed','Failed'));
                disp(varTabRm);
            end
            if (nIm > 0) && (nRm > 0)
                anovaType   = sprintf('%d-way mixed', nIndep);
            elseif (nIm > 0)
                anovaType   = sprintf('%d-way independent-measures', nIm);
            else
                anovaType   = sprintf('%d-way repeated-measures', nRm);
            end
            fprintf("ANOVA test (%s): \t%s\n", anovaType, local.ifelse(any(anovaTab.isSig),'Passed','Failed'));
            disp(anovaTab);
        end

        % Package the result into an array
        results(iDep).norm  = normTab;
        results(iDep).varIm = varTabIm;
        results(iDep).varRm = varTabRm;
        results(iDep).anova = anovaTab;
        results(iDep).model = rmMod;
    end
end

%% Local functions
function name = funcname()
    % Description:  Query the name of the current function.
    stackTrace  = dbstack();
    name        = stackTrace(2).name;
end

function out = varname(~)
    % Description:      Convert a variable name to a string.
    out         = inputname(1);
end

function parser = addParameters(parser, defaultPars)
    % Description:  Add optional-name value parameters to an inputParser
    %               object and assign the default value.
    for parName = fieldnames(defaultPars)'
        parser.addParameter(parName{:}, defaultPars.(parName{:}));
    end
end

function colName = colIndexToName(tabData,colIndex)
    % Description:  Table columns can be indexed with logical arrays,
    %               numerical indices, or field names. For any of these
    %               indexing options, check that they are valid and return
    %               the corresponding field names as a cellstr row array.
    nCols       = size(tabData,2);
    colNames    = tabData.Properties.VariableNames;
    varName     = inputname(2);
    
    if isempty(colIndex)
%         error("Input '%s' must be supplied.", varName);
        colName     = {};
    elseif islogical(colIndex)
        % Variable indexed as logical array
        if numel(colIndex) ~= nCols
            error("Input '%s' (logical array) must have as many elements as table columns.", varName);
        end
        colName     = colNames(colIndex);
    elseif isnumeric(colIndex)
        % Variable indexed as column indices
        if max(colIndex) > nCols
            error("Input '%s' (numerical indices) cannot be greater than the number of table columns.", varName);
        end
        colName     = colNames(colIndex);
    elseif ischar(colIndex) || isstring(colIndex) || iscellstr(colIndex)
        % Variable indexed as case-sensitive text
        isMember    = ismember(colIndex, colNames);
        if any(~isMember)
            error("Input '%s' (field names) not found: %s.", varName, strjoin(colIndex(~isMember),', ') );
        end
        colName     = reshape( cellstr(colIndex), 1, [] );
    else
        error("Input '%s' has unexpected type ('%s'). Expected: logical, numeric, text (char, cellstr, string).", varName, class(colIndex) );
    end
end

function [indLogical, indNumeric] = fieldToIndex(tabData,field)
    % Description:  Convert table field indices to logical and numeric
    %               indices.
    tabFields   = tabData.Properties.VariableNames;
    indLogical  = ismember(tabFields, field);% Index in 'tabData'
    [~,indNumeric]      = ismember(field, tabFields);
end
