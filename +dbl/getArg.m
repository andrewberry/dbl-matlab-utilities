% function varargout = getArg(indsOut,func,varargin)
function varargout = getArg(outArgInds,varargin)
    % Description:      Select and/or capture specific output arguments.
    % Syntax:
    %   1) [out1,...,outN] = getArg(outArgInds,func,funcArg1,...,funcArgN)
    %   2) out1 = getArg(outArgInds,func,funcArg1,...,funcArgN)
    %   3) out1 = getArg(outArgInds,'OutputType',outType,func,funcArg1,...,funcArgN)
    % Inputs:
    %   outArgInds  :   Numerical indices of output arguments to return. 
    %                   Returned arguments will be in the specified order. 
    %                   Repeated values are permitted. 
    %                           Type: double (array).
    %   options     :   Optional struct or name-value input pairs
    %                     changing the user setting.
    %   func        :   Function handle of the target function.
    %                           Type: function handle.
    %               
    %   funcArg1...N :  Zero or more input arguments to the function. 
    % Outputs:
    %   out1...N    :   Depending on the inputs, either 
    %                   (1) a single cell array of all output arguments, or 
    %                   (2) multiple separately-assigned output arguments
    %                       (default).
    % Examples:
    % (1) Basic example: Function 'min' returns the minimum value and its
    %   index as the 1st and 2nd output arguments, respectively. In this
    %   example, we retrieve the 2nd, 1st, and 2nd (again) arguments in
    %   that order.
    %   [out1 out2 out3] = getArg([2,1,2], @min, [5 2 3 9 13 4 1 7 8]);
    %   >> out1 = 7
    %   >> out2 = 1
    %   >> out3 = 7
    % (2) Same example, but outputting a cell array:
    %   out1 = getArg([2,1,2], 'outputCell',true, @min, [5 2 3 9 13 4 1 7 8]);
    %   >> out1 = {7,1,7}
    %
    % History:
    % * 2020/09/04: Created, A.Berry. Written for selecting outputs from 
    %               the 'ind2sub' function.
    % * 2020/10/21: Options added to consistently output a cell array.
    %               Rewrote the function to extract all funciton outputs 
    %               into a buffer prior to rearranging for assignment.
    % * 2020/11/24: Optional cell array output is now given the same shape
    %               as the arg index list. This can potentially make it
    %               easier to embed in an arrayfun or cellfun loop.
    % Notes:
    % * Matlab has a coding competition for this problem: 
    %       https://www.mathworks.com/matlabcentral/cody/problems/266
    %   However, the solutions are not visible unless you create a better
    %   solution. The implementation here is a hacky attempt at creating it,
    %   using the same syntax and examples as the competition.
    % * Variable number of output arguments in Matlab:
    %   - Keyword 'nargout' returns the number of output arguments on the
    %     left-hand side of the assignment expression. It can be zero or
    %     more.
    %   - Keyword 'varargout' is a cell array containing all output
    %     arguments. It should be of size 'nargout' or greater.
    %   - If nargout <= numel(varargout), then the first 'nargout'
    %     arguments will be returned. Exception: If 'nargout' is zero and
    %     the function call is not terminated with a semicolon, then the
    %     first argument will be returned.
    %   - If nargout > numel(vargout), then an error is thrown.
    % * Text-to-command function 'eval' does not compile at run-time, so is
    %   quite slow to execute. It also results in code with poor readability.

    % Default options
    opts.outputCell     = false;
    
    %%% Parse input arguments
    
    % Separate the inputs into options, funciton handle, and function
    % arguments.
    % * Anything before the first function handle is an option; anything
    %   after is a function argument.
    indFunc             = find(cellfun(@(x) isa(x,'function_handle'), varargin), 1, 'first');
    funcHandle          = varargin{indFunc};
    funcInputs          = varargin((indFunc+1):end);
    optArgs             = varargin(1:(indFunc-1));
    
    % Parse options inputs
    % * Load any (partial) options structure and overwrite defaults.
    % * Append additional name-value input arguments.
    isStruct    = cellfun(@isstruct, optArgs);
    if any(isStruct)
        optsUser    = optArgs{find(isStruct,1,'first')};
        for optField = fieldnames(optsUser)'
            opts.(optField{:})  = optsUser.(optField{:});
        end
    end
    for field = fieldnames(opts)'
        if any(strcmpi(optArgs, field))
            opts.(field{:})     = optArgs{find(strcmpi(optArgs,field{:}),1,'first') + 1};
        end
    end
    
    
    %%% Retrieve output arguments and re-assign as specified
    
    % Error-checking: 
    % * Throw error if assignment uses more arguments than are specified by
    %   'outArgInds'. If fewer arguments are assigned, then only the first
    %   'nargout' outputs are assigned (additional are ignored).
    if nargout > numel(outArgInds)
        error('Too many output arguments (max %d).',numel(outArgInds));
    end
    
    % Create a buffer for capturing the relevant output arguments
    % * Create string of multiple output variables.
    % * Invoke string argument assignment with 'eval' statement.
    funcOutInds = unique(outArgInds);% Sorted indices with duplicates removed.
    funcOutStr  = repmat({'~'},1,max(outArgInds));
    funcOutStr(funcOutInds)     = arrayfun(@(x) sprintf('funcOutputs{%d}',x),funcOutInds,'UniformOutput',false);
    funcOutStr  = ['[',strjoin(funcOutStr,','),']'];% e.g., '[~,funcOut{2},funcOut{3},~,funcOut{5}]'
    eval([funcOutStr,' = funcHandle( funcInputs{:} );']);
    
    % Return the requested arguments in the specified format
    if opts.outputCell
        % Output a single cell array of all arguments
        % * Give the output the same shape as the list of argument indices
        varargout{1}    = reshape(funcOutputs(outArgInds),size(outArgInds));
    else
        % Assign output arguments separately
        varargout       = funcOutputs(outArgInds);
    end
end