function res = ifelse(condition, resTrue, resFalse)
    % Description:  One-line if/else statement for variable assignment.
    if condition
        res     = resTrue;
    else
        res     = resFalse;
    end
end