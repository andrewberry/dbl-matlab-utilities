function permList = permuteVars(varList, direction)
    % Description:  Recursively generate a flattened tree structure of all  
    %               permutations of repeating cell array contents.
    % Input:
    %   varList:    Nested cell array of variables to permute. The top level
    %               has the number of dimensions to combine. Each dimension
    %               contains a list of the names of all possible children.
    %               The top level recurses in reverse order.
    %   direction:  (Optional) {'reverse' (default), 'forward'} The order
    %               in which to group permutations of the input elements.
    %   permList:   Cell arrary of the expanded tree structure of size
    %                   [nPermutations, nLevels]
    % Example 1:
    %   varList  = {{'A','B'},{'C','D','E'}};
    %   permList = permuteVars(varList);
    %     {'A'}    {'C'}
    %     {'A'}    {'D'}
    %     {'A'}    {'E'}
    %     {'B'}    {'C'}
    %     {'B'}    {'D'}
    %     {'B'}    {'E'}
    %
    % Example 2:
    %   varList  = {{'A','B'},{'C','D','E'}};
    %   permList = permuteVars(varList,'forward');
    %     {'A'}    {'C'}
    %     {'B'}    {'C'}
    %     {'A'}    {'D'}
    %     {'B'}    {'D'}
    %     {'A'}    {'E'}
    %     {'B'}    {'E'}
    %
    % History:
    % * 2020/08/28: Created, A. Berry.
    % Notes:
    % * Reversing the order can alternatively be accomplished by reversing
    %   the order of columns of the input and output, e.g.:
    %       permList    = fliplr(permuteVars( fliplr(varList) ));

    local       = module();
    
    if ~exist('direction','var') || isempty(direction)
        direction   = 'reverse';
    end
    
    nLayers     = numel(varList);
    if nLayers == 0
        permList    = {};
    elseif nLayers == 1
        permList    = reshape(varList{1},[],1);% Force column output
    else% Recurse to the next group variable
        
        if strcmpi(direction,'forward')
            superLayer  = local.permuteVars(varList(1:(end-1)),direction);
            nSuperRows  = size(superLayer,1);

            children    = reshape(varList{end},[],1);% Force column output
            nChildren   = numel(children);
            repChildren = reshape(repmat(children',nSuperRows,1),[],1);
            permList    = [repmat(superLayer,nChildren,1), repChildren];
            
        else% 'reverse' (default)
            subLayer    = local.permuteVars(varList(2:end),direction);
            nSubRows    = size(subLayer,1);

            children    = reshape(varList{1},[],1);% Force column output
            nChildren   = numel(children);
            repChildren = reshape(repmat(children',nSubRows,1),[],1);
            permList    = [repChildren, repmat(subLayer,nChildren,1)];
        end
    end
end