function cells = num2cellstr(nums)
    % Description:  Convert a numeric array into a cell array of strings.
    %               This can be useful for string concantenation or for
    %               creating categorical arrays (the 'categorical' data
    %               type is not supported by many of Matlab's functions and
    %               methods).
    % Inputs:
    %   nums:       An mxn numeric array.
    % Outputs:
    %   cells:      An mxn cell array of strings.
    % Notes
    % * In the case of 1D numeric arrays, the following may be an 
    %   alternative:
    %       cellstr(num2str(reshape(nums,[],1)))% Column vector output
    cells   = arrayfun(@(x) num2str(x), nums, 'UniformOutput',false);
end