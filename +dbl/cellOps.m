function resultCell = cellOps(varargin)
    % Description:  Perform arbitrary operations on two or more cell
    %               arrays.
    % Syntax:
    %   1) resultCell = cellOps(cell1,cell2,operator1)
    %   2) resultCell = cellOps(cell1,operator1,cell2,operator2,cell3,...)
    % Inputs:
    %   operands    :   Two or more operands consisting of similarly-sized
    %                   cell arrays. The contained data can be anything,
    %                   provided that it is a consistent type, consistent
    %                   dimension, and is compatible with the specified
    %                   operator. 
    %   operators   :   One or more bivariate operators input as function
    %                   handles, with the following resultant behaviour:
    %                   1) A single operator is supplied and applied to all
    %                       operands recursively.
    %                   2) Different operators are used, in sequential
    %                       order. There must be one fewer operator than
    %                       operands.
    %                   It is assumed that the operator is appropriately
    %                   selected for the underlying data type contained
    %                   within the cells.
    %                   Examples: https://nl.mathworks.com/help/matlab/matlab_oop/implementing-operators-for-your-class.html
    %                       * General: @plus, @minus, 
    %                       * Element-wise: @times, @power, @ldivide
    %                       * Matrix: @mtimes, @mpower, @mldivide
    %                   Precedence is from left to right; grouping can be 
    %                   achieved by repeated calls to this function or by
    %                   creating a class and overloading the relevant 
    %                   symbolic operators (e.g., '+', '-', '*', '/').
    % Outputs:
    %   resultCell  :   Cell array of the same size as the input containing
    %                   the result of the specified operation.
    % Examples:
    % 1) Subtract the contents of one multi-dimensional cell array from
    %   another.
    %   >> A = num2cell(reshape(1:6,3,[]));
    %   >> B = num2cell(cell2mat(A).^2);
    %   >> C = cellOps(A,B,@minus);
    % 2) Chain together multiple operations
    %   >> C = cellOps(A,@times,B,@minus,A,@plus,3.14);
    % Notes:
    % * Later extend functionality to include unary operations. Perhaps is 
    %   better to just make a new class wither overloaded operators, and 
    %   just type cast back and forth with cell.
    %   https://nl.mathworks.com/help/matlab/matlab_oop/implementing-operators-for-your-class.html
    % History:
    % * 2020/10/22: Created, A.Berry. Designed for removing baseline values
    %   from numeric data arranged in cell arrays for plotting.
    
    isFunc      = cellfun(@(x) isa(x,'function_handle'), varargin);
    operators   = varargin(isFunc);
    operands    = varargin(~isFunc);
    switch numel(operators)
        case 0
            error('No operator argument identified.');
        case 1
            % Single operator: duplicate to conform to syntax.
            operators   = repmat(operators,numel(operands)-1,1);
        case numel(operands)-1
            % Fully-specified: Ok as-is.
        otherwise
            error(['Unexpected number of operators supplied. ',...
                'Expected %d, received %d.'], numel(operands)-1, numel(operators));
    end
    
    % For any non-cell operands, distribute the value to a similarly-sized
    % cell array
    isCell  = cellfun(@iscell, operands);
    szCell  = size(operands{find(isCell,1,'first')});
    operands(~isCell)   = cellfun(@(x) repmat({x},szCell), operands(~isCell),'UniformOutput',false);
    
    resultCell     = operands{1};
    for iOperand = 2:numel(operands)
        for iArray = 1:numel(resultCell)
            resultCell{iArray} = operators{iOperand-1}(resultCell{iArray},operands{iOperand}{iArray});
        end
        % Alternatively, can be arrayfun call instead of for-loop.
    end
end