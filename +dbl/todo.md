# To-do DBL Matlab package

## General
* [x] Come up with a short and descriptive name for the statistics/plotting package: statplot, splot, staplot, stapl

## Statistics
* [ ] Global significance tests
    - [x] Parametric qualifier tests: normality test, homogeneity test, sphericity test
    - [x] Parametric tests: independent-, repeated-measures
    - [ ] Non-parametric tests: independent-, repeated-measures
    - [x] Effect size
    - [ ] Check inputs
        * [x] Use inputparser.
        * Independent variables (imVars, rmVars) must be categorical. Check `iscategorical(x) || ischar(x) || isstring(x) || iscellstr(x)`. Cast to `cellstr` for easier string operations. Be careful about casting empty values, since result is not always as expected.
        * Check at least one independent variable
        * Check exactly 1 dependent variable and is numeric.
        * Empty entries: throw error or remove row?
    - [ ] Unstacking of RM variables: Current implementation might result in non-existent combinations.
        * Concatenate rmVars as strings, find the unique values, then split again into the constituent levels. These levels are used to define Within-Subjects factors.
        * Error-checking: Check that number of instances of concatenated rmVar is the same for all unique values. `[keys,~,indKeys] = unique(x); counts = accumarray(indKeys,1);`, where `counts` is a `numel(keys)` x 1 array counting the occurrence of each index.
        * Error-checking: Check that amount of data does not change. Data should start out as a single column, then end up with more columns but fewer rows. Skip empties and NaN. NaN is automatically added if unstacking does not work appropriately.
* [x] Pairwise significance tests: see also Matlab function `multcompare`
    - [x] Parametric qualifier tests
    - [x] Parametric tests: independent-, repeated-measures
    - [x] Non-parametric tests: independent-, repeated-measures
    - [x] Effect size
    - [x] Type-I correction for multiple comparisons (Bonferroni)

* [ ] Interpretation aids
    - Text reporting of main/interaction effects
    - Effect size translation (Cohen)
    - Parametric qualifier test special cases: 
        * Data guaranteed to be spherical when only 2 levels (Matlab output shows failure, however).

## Plotting
Plotting questions:
* Which stats to display and when? E.g., mean + SD, median + CI, ...
    - See notes in `Statistics.md / Plotting`
    - References:
        * Streit2014 [Nature article](http://www.nature.com/articles/nmeth.2807), 
            - Avoid bar graphs, since they can be falsely interpreted as indicating a sampled range; do not show shape of the distribution.
        * Field2002
            - Parametric: Bar graphs with mean + symmetric 95% CI (Field2002 p135, 176, 192). Line plots with mean + 1 or 2 standard errors (p135), corresponding to 68% and 95% CI, respectively.
            - Non-parametric (Field2002 p237): Median is more relevant than mean because of outliers; mean no longer used in tests. Whiskered box plot, showing the median, 50% CI (box) and min/max within +/- 3 SD (whiskers).
        * General Googling
            - For parametric, mean + SD are relevant but insufficient to show the true distribution.
            - Histograms are best for showing the distribution, but inconvenient for displaying multiple distributions. Violin plots are also good (like box plot, but with shape corresponding to the distribution).
* Violin plots
    - Wikipedia Violin plot: https://en.wikipedia.org/wiki/Violin_plot
    - Pro: Show complete distribution (incl modality, similar to histogram)
    - Con: Clarity of properties (outliers, median/mean, quantiles)
    - Implemented in Matlab
        1. https://github.com/bastibe/Violinplot-Matlab
            - Shows median, mean (optional), boxplot overlay.
            - Pro: Attractive, nice coding style.
            - Con: Limited features. No automatic grouping, no manual positioning of x-values, no manual changing colours (can change axis ColorOrder or patch handles post-hoc), violin width can only be specified wrt max distribution density.
        2. https://nl.mathworks.com/matlabcentral/fileexchange/45134-violin-plot
        3. https://nl.mathworks.com/matlabcentral/fileexchange/23661-violin-plots-for-plotting-multiple-distributions-distributionplot-m
        4. https://nl.mathworks.com/matlabcentral/fileexchange/72424-violin


* [ ] Automatic grouping/tiling of table data
* [ ] Automatic table to cell
* [ ] Significance brackets
* [ ] n-d grouped boxplot, scatterbox with settings and object handles
* [ ] n-d grouped bar plot w/ whiskers
* [ ] Shaded lineplot with optional statistics: 
    - Representative line: mean, median
    - Dispersion area: SD (#), SE (#), CI

## Notes
Standardizing plotting syntax:
* Grouped plots: M clusters of N members.
* `bar(y)`, `bar(x,y)`
    - `y` is MxN array of scalars, resulting in M clusters of N members/bars.
* `boxplot2(y,clusterTickLabels,memberLabels,colours)`
    - `y` is MxN cell array of vectors, resulting in M clusters of N members/boxes.
* `boxplot`: Grouping can be emulated by specifying position. Accepts several different data types.

Implementation:
* `bar`:
    - Creates bars individually by looping over series (line 228). Primitive plot objects are generated one at a time, automatically resizing and repositioning if the location is not manually specified. numSeries is N in this case.
    - Position and width of bars automatically determined (and updated) by the primitive object class (line 241).
* `boxplot`:
* `boxplot2`:
    - Calls `boxplot` then extracts the box vertices and places a patch over top. This hides the line drawing, so transparency is used. A better solution is to reorder the graphics objects so that the patches are at the back.
    - Position of boxes: Create a list of (nClusters)x(nSeries+1) evenly-spaced points, delete every (nSeries+1)th point, re-center on cluster indices.
    - Refs:
        * https://nl.mathworks.com/matlabcentral/answers/304599-how-to-plot-using-hierarchically-grouped-boxplot
        * https://github.com/kakearney/boxplot2-pkg
    - I previously used this as inspiration for `scatterbox`.
* `violinplot`:
    - Limited options arguments, but nice modularity, data type flexibility, and use of Matlab input parser. Good example for best-practice.
    - Refs: https://github.com/bastibe/Violinplot-Matlab

Concatenation in Matlab, e.g., `y(m,n) = y(3,2)`
* `y(:) = reshape(y,[],1) = [y(1,1),y(2,1),y(3,1), y(1,2),y(2,2),y(3,2)]'`

Examples of input parser usage:
* Violinplot
* Boxplot2 (Kelly Kearney)