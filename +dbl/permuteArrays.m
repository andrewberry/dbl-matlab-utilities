function varargout = permuteArrays(array, options)
    % Description:  Find all combinations of elements of 2 or more arrays.
    % Syntax:
    %   * perms = permuteArrays(array1, array2)
    %           Inputs 'array1' and 'array2' are permuted. Output 'perms'
    %           is a cell array containing the matched array elements.
    %   * __ = permuteArrays(array1, ..., arrayN)
    %           Permutations of multiple variables.
    %   * [perms1, ..., permsN] = permuteArrays(array1, ..., arrayN)
    %           The output is unpacked into separate arrays.
    %   * __ = permuteArrays(__, Name, Value)
    %           Optional name-value arguments are provided.
    % Inputs:
    %   array1...N  :   Arrays to permute.
    % Optional name-value inputs:
    %   SortOrder   :   The order in which to permute the input arrays. 
    %                   'first' (default): The output is grouped by values
    %                           of the first input.
    %                   'last': The output is grouped by values of the
    %                           last input.
    % Outputs:
    %   perms       :   Cell array of the permuted input arrays
    %                   (nPermutations x nArrays)
    %   perms1...N  :   Alternative output format in which the ith output
    %                   argument corresponds to the permutations of the ith
    %                   input array.
    % Examples:
    % * Example 1:
    %   >> perms    = dbl.permuteArrays({'A','B'}, {'C','D','E'});
    %   >> disp([perms{:}])
    %     {'A'}    {'C'}
    %     {'A'}    {'D'}
    %     {'A'}    {'E'}
    %     {'B'}    {'C'}
    %     {'B'}    {'D'}
    %     {'B'}    {'E'}
    %
    % * Example 2: Different sorting order and different number of outputs.
    %   >> [perms1,perms2] = dbl.permuteArrays(["A","B"], ["C","D","E"], 'SortOrder', 'last');
    %   >> disp([perms1, perms2])
    %     "A"    "C"
    %     "B"    "C"
    %     "A"    "D"
    %     "B"    "D"
    %     "A"    "E"
    %     "B"    "E"
    % History:
    % * 2021/02/26: Created, A.Berry. Wrapper for 'permuteVars.m' to
    %               produce a simpler syntax, similar to
    %               'permuteArrays2.m'.

    % Parse the input arguments and assign default values
    % * Repeating arguments are packaged into a cell array
    arguments (Repeating)
        array               {mustBeNonempty}
    end
    arguments
        options.sortOrder   string {mustBeText,mustBeScalarOrEmpty} = "first"
    end
    
    local       = module();
    
    % Check 'SortOrder'
    switch lower(options.sortOrder)
        case "last"
            recursionOrder  = "forward";
        case "first"
            recursionOrder  = "reverse";
        % other sorting external to 'permuteVars'? E.g., see 'sort.m'
        otherwise
            error("Argument to 'SortOrder' not recognized.");
    end
    
    nArrays         = numel(array);
    permList        = local.permuteVars(array, recursionOrder);
    nPerms          = size(permList,1);
    
    % Assign output arguments
    outArgs         = mat2cell(permList, nPerms, ones(nArrays,1));
    if (nargout > 1) || (nArrays == 1)
        % Output arguments are assigned individually
        varargout       = outArgs;
    else
        % Output arguments are assigned as a single cell array
        varargout{1}    = outArgs;
    end
end

%% Local functions
function mustBeText(value)
    % Description:  Validation function for any class type. For some reason
    %               this one wasn't implemented for R2020a, but is for
    %               R2020b.
    isClass     = ischar(value) || iscellstr(value) || isstring(value);
    if ~all(isClass)
        error("Value must be text.");
    end
end