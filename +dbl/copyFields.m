function objC = copyFields(objA,objB,varargin)
    % Description:  Merge data structure B into A.
    % Inputs:
    %   objA    : Struct or table
    %   objB    : Struct or table
    % Name-value optional arguments:
    %   mergeType   : Function to use to determine the fields included in
    %                 the output data structure. Values:
    %                   'A': Fields in A.
    %                   'B': Fields in B.
    %                   'A&~B': Fields in A but not B (same as 'setdiff').
    %                   'B&~A': Fields in B not A (same as 'setdiff' with
    %                           the arguments interchanged).
    %                   'union': (default) All fields from both A and B.
    %                   'intersect': Only fields shared by both A and B.
    %                   'setdiff': Only fields in A not in B.
    %                   'setxorr': Only fields not shared by both A and B.
    %                   More set operations: https://nl.mathworks.com/help/matlab/set-operations.html
    %                   Function handle: e.g., @union, or anonymous
    %                           function with 2 inputs such as @(A,B) A
    %   priority    : In the event of fields shared by both A and B, select
    %                 which to copy. Values: 'A' (default) or 'B'.
    %   outputType  : The type of the output data structure. Values:
    %                   'inherit': (default) The data type of the object 
    %                               specified by 'priority'.
    %                   'struct' : Struct
    %                   'table'  : Table
    % Outputs:
    %   objC    : Struct or table, as specified by the inputs
    % Examples:
    %   >> objA = struct('a',1,'b',2);
    %   >> objB = struct('b',3,'c',4);
    %   >> objC = copyFields(objA,objB)% mergeType='union', priority='A'
    %           a: 1 % A
    %           b: 2 % A
    %           c: 4 % B
    %   >> objC = copyFields(objA,objB,'priority','B')
    %           a: 1 % A
    %           b: 3 % B
    %           c: 4 % B
    %   >> objC = copyFields(objA,objB,'mergeType','intersect','priority','B')
    %           b: 3 % B
    %   >> objC = copyFields(objA,objB,'mergeType',@(A,B) A(strcmpi(A,'a')) )
    %           a: 1 % A
    % Notes:
    % * To do: 
    %   - Implement override feature for selectively copying specific fields
    %   - Add functionality for struct arrays.
    %       * Require similarity of array size: numel(objA) == numel(objB)
    % History:
    % * 2020/09/04: Created, A.Berry.
    
    % Default options
    opts.mergeType          = 'union';
    opts.priority           = 'A';
    opts.outputType         = 'inherit';
    opts.fieldList          = {};% Overrides mergeType if not empty.
    
    % Parse optional name-value input arguments
    for field = fieldnames(opts)'
        if any(strcmpi(varargin, field))
            opts.(field{:})     = varargin{find(strcmpi(varargin,field{:}),1,'first') + 1};
        end
    end
    
    % Check data type and exctract field names from both objects
    if isstruct(objA)
        fieldsA     = fieldnames(objA);
    elseif istable(objA)
        fieldsA     = objA.Properties.VariableNames;
    else
        error("Unexpected data type for 'objA'.");
    end
    if isstruct(objB)
        fieldsB     = fieldnames(objB);
    elseif istable(objB)
        fieldsB     = objB.Properties.VariableNames;
    else
        error("Unexpected data type for 'objB'.");
    end
    
    if isa(opts.mergeType,'function_handle')
        fieldsC     = opts.mergeType(fieldsA,fieldsB);
    elseif strcmpi(opts.mergeType,'A')
        fieldsC     = fieldsA;
    elseif strcmpi(opts.mergeType,'B')
        fieldsC     = fieldsB;
    elseif strcmpi(opts.mergeType,'A&~B')
        fieldsC     = setdiff(fieldsA,fieldsB);
    elseif strcmpi(opts.mergeType,'B&~A')
        fieldsC     = setdiff(fieldsB,fieldsA);
    else
        mergeFunc   = str2func(opts.mergeType);
        if isempty(functions(mergeFunc))
            error("Merge function '%s' not found.",opts.mergeType);
        end
        fieldsC     = mergeFunc(fieldsA,fieldsB);
    end
    
    if ~any(strcmpi(opts.priority,{'A','B'}))
        error("Option 'priority' may only have the following values: 'A', 'B'.");
    end
        
    % Copy the fields
    if strcmpi(opts.priority,'B')
        for fA = intersect(fieldsC,fieldsA)'
            objC.(fA{:})    = objA.(fA{:});
        end
        for fB = intersect(fieldsC,fieldsB)'
            objC.(fB{:})    = objB.(fB{:});
        end
    else
        for fB = intersect(fieldsC,fieldsB)'
            objC.(fB{:})    = objB.(fB{:});
        end
        for fA = intersect(fieldsC,fieldsA)'
            objC.(fA{:})    = objA.(fA{:});
        end
    end
    
    % Set the output datatype
    if ( strcmpi(opts.outputType,'inherit') &&...
    ( (strcmpi(opts.priority,'A') && istable(objA)) ||...
    (strcmpi(opts.priority,'B') && istable(objB)) ) ) ||...
    strcmpi(opts.outputType,'table')
        objC    = struct2table(objC);
    end
end