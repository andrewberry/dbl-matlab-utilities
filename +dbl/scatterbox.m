function h = scatterbox(plotdata,x,varargin)
    % Description:      Something between a scatter plot and a boxplot,
    %                   with nice transparent shaded surfaces.

    %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    % Hard-coded variables
    % https://nl.mathworks.com/help/matlab/ref/linespec.html
    markers     = {'.'};
% %     markers     = {'+','o','*','.','x','s','d','^','v','>','<','p','h'};
%     markers     = {'+','.','*','o','x','s','d','^','v','>','<','p','h'};
% %     markers     = {'o','s','d','^','v','<','>'};
% %     markers     = {'+','o','*','x','s','d','^','v','>','<','p','h'};
    
    %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    % Process input arguments

    % [X x group x Y]
    [nX,nG,nY]  = size(plotdata);
    if ~exist('x','var') || isempty(x)
        x   = (1:nX)';
    end 
    if any(strcmpi(varargin, 'BoxWidth'))
        boxProp.width   = varargin{find(strcmpi(varargin,'BoxWidth'),1,'last') + 1};
    else
        boxProp.width   = 0.8;
    end
    if any(strcmpi(varargin, 'MarkerOffset'))
        mrkProp.offset  = varargin{find(strcmpi(varargin,'MarkerOffset'),1,'last') + 1};
    else
        mrkProp.offset  = zeros(1,nX); % 0.2*[-1,1];
    end
    if any(strcmpi(varargin, 'MarkerDispersionRatio'))
        mrkProp.dispersionRatio  = varargin{find(strcmpi(varargin,'MarkerDispersionRatio'),1,'last') + 1};
    else
        mrkProp.dispersionRatio  = 1;
    end
   
    
    % Change the default behaviour of the axes from cycling through colours
    % to cycling through markers.
    h.Ax        = gca;
    colorOrder  = h.Ax.ColorOrder;
    set(h.Ax,   'LineStyleOrder',   markers,...     % Turn on auto marker change
                'ColorOrder',       [0,0,0],...     % Turn off auto colour change
                'NextPlot',         'replacechildren');
    
    % To get the x-positions of the groups, make a bar plot and then delete
    % it.
    hBar                = bar(x,plotdata(:,:,1));
    boxProp.offset      = [hBar.XOffset];
    delete(hBar);
    
    % Rescale box width if number of groups or x spacing grows.
    if nG > 1
        boxProp.width   = boxProp.width*min(diff(boxProp.offset));
    else
        boxProp.width   = boxProp.width*min(diff(x));
    end
    
    dispersionLims      = 0.5*boxProp.width*mrkProp.dispersionRatio*[-1,1];
    mrkProp.dispersion  = linspace(dispersionLims(1), dispersionLims(2), nY);
    
    %~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    % Plot the boxes and markers
    hold on;
    for iG = 1:nG
        % Plot the boxes
        for iX = 1:nX
            h.xPos(iG,iX)   = x(iX) + boxProp.offset(iG);
            xPos            = h.xPos(iG,iX) + 0.5*boxProp.width*[-1,1];
%             h.Box(iG,iX)     = addBox(xPos, [min(plotdata(iX,iG,:)),max(plotdata(iX,iG,:))], 'EdgeColor',colorOrder(iG,:), 'FaceColor',colorOrder(iG,:), 'FaceAlpha',0.1);
            h.Box(iG,iX)     = addBox(xPos, [min(plotdata(iX,iG,:)),max(plotdata(iX,iG,:))], 'EdgeColor',[1,1,1], 'FaceColor',colorOrder(iG,:), 'FaceAlpha',0.1);
            h.Line(iG,iX,:)  = plot(xPos',[min(plotdata(iX,iG,:))*ones(size(xPos)); max(plotdata(iX,iG,:))*ones(size(xPos))]', '-', 'Color',colorOrder(iG,:));
        end
        % Plot the markers
        xPos        = repmat(x,1,nY) + boxProp.offset(iG) + mrkProp.dispersion + mrkProp.offset(iG);
        h.Plot(iG,:) = plot(xPos, squeeze(plotdata(:,iG,:)), 'Color',colorOrder(iG,:), 'MarkerSize',7, 'LineWidth',1.5);
%         for iY = 1:nY
%             xPos    = x + boxProp.offset(iG) + mrkProp.dispersion(iY) + mrkProp.offset(iG);
%             hPlot(iG,iY)    = plot(xPos, squeeze(plotdata(:,iG,iY)), 'Color',colorOrder(iG,:), 'MarkerSize',7, 'LineWidth',1.5);
%         end
% %         hAx.LineStyleOrderIndex     = 1;  % Reset marker counter for next group
    end
    hold off;
%     set(h.Ax,   'LineStyleOrder',   '-',...             % Turn off auto marker change
%                 'ColorOrder',       colorOrder,...      % Turn on auto colour change
%                 'NextPlot',         'replacechildren');
% %     uistack(hPlot,'top');  % Put selected axis on top
end

function hBox = addBox(xBorders, yBorders, varargin)
    % Description:  Wrapper function for 'patch' to avoid annoying
    %               conversion from shape vertices to filled surface. This
    %               version can only draw normally-oriented rectangles.
    % Inputs:
    %   xBorders    <2x1/1x2 double> Lower and upper X-limits of the box.
    %   yBorders    <2x1/1x2 double> Lower and upper Y-limits of the box.
    %   varargin    Linespec properties, see: https://nl.mathworks.com/help/matlab/ref/patch.html
    % Example:
    %   addBox(0.5*[-1,1],[0,1], 'EdgeColor',[1,0,0], 'FaceColor',[1,0.8,0.8], 'FaceAlpha',0.5);
    
    [xData,yData] = meshgrid(xBorders,yBorders);    % x-constant: columns. y-constant: rows.
    yData(:,2)    = flip(yData(:,2));

    hBox  = patch('XData',xData(:), 'YData',yData(:), varargin{:});
end

% function addBox(posData,varargin)
%     xyLL    = posData(1:2); % Lower-left vertex
%     w       = posData(3);
%     h       = posData(4);
%     
%     rectangle('Position',[xyLL, w, h],'EdgeColor',[1,0,0]);
% end