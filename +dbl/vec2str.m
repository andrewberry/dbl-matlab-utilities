function str = vec2str(vec,delim)
    % Description:  Print a vector as a string with optional delimiters.
    
    if ~exist('delim','var')% || isempty(delim)
        delim   = '[]';% 1x2 char array
    end

    if ~iscell(vec)
        str     = sprintf([repmat('%g, ', 1, numel(vec)-1),'%g'], vec);
    else% Cell array, presume string contents.
        str     = sprintf([repmat('%s, ', 1, numel(vec)-1),'%s'], vec{:});
    end
    
    if numel(delim)==2
        str     = [delim(1),str,delim(2)];
    end
end