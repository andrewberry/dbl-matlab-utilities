function [pairStats,normStats,varStats] = pairTest(data,varargin)
% function [pairStats,normStats,varStats] = pairTest(data,pairs,isIndep,testType)
    % Description:  Statistical comparisons of paired distributions.
    %               Includes tests for both parametric and non-parametric
    %               assumptions and for both independent- and
    %               repeated-measures variables. Parametric assumptions can
    %               be automatically checked and assigned.
    % Inputs:
    %   data    :   Data input in one of the following formats. All
    %               underlying data must be numeric and 1D.
    %                   Array       : Data is grouped across the first
    %                                 dimension (e.g., columns are groups).
    %                   Cell array  : Each cell represents a distinct 
    %                                 group.
    %                   table, struct : Each field represents a distinct
    %                                 group.
    %                   For 1D (cell) array input, N categorical variables
    %                   can be used to group data into an N-D array.
    %                   Additional inputs can be used to skip or reorder
    %                   entries; see 'data2cell.m' for more info.
    % Optional inputs:
    %   pairs   :   Np x 2 array indicating the indices of pairs to
    %               compare, where Np is the numper of pairs. Its contents
    %               depend on the type of 'data':
    %                   2D numeric      : Column indices, e.g., [1,3]
    %                   2D cell         : 1D or 2D cell indice, e.g., {{1,2},{3,4}}
    %                   table, struct   : Field names, e.g., {'meas1','meas5'}
    %               Default: All unique combinations of data groups are
    %               compared.
    %   isIndependent : Logical singleton or Np x 1 logical array indicating 
    %               whether pairs are indepedent- (true) or repeated-
    %               (false) measures.
    %               Default: true (independent-measures assumed).
    %   normalityAssumption : String indicating the parametric assumptions. Values:
    %                   'auto'          : Automatically check for normality
    %                                     amongst the selected pairs. Note
    %                                     that global normality tests might
    %                                     find non-normality across other
    %                                     pairs.
    %                   'parametric'    : Assume parametric.
    %                   'nonparametric' : Assume non-parametric.
    %               Default: 'auto'.
    %   normTestFunc: Function handle to a univariate test for normality.
    %               Values:
    %                   @kstest     : Kolmogorov-Smirnov test (built-in)
    %                   @lillietest : Lilliefors test (built-in)
    %                   @adtest     : Anderson-Darling test (built-in)
    %                   @swtest     : Shapiro-Wilk test (external)
    %               Default: @swtest.
    %   varTestName : Name of the method for testing homogeneity of
    %               variance across independent-measures variables.
    %               Values (see 'vartestn.m'):
    %                   'Bartlett'
    %                   'LeveneQuadratic'
    %                   'LeveneAbsolute'    : Default. Used in SPSS.
    %                   'BrownForsythe'
    %                   'OBrien'
    %   alpha   :   Fisher's significance criterion, used as a threshold
    %               for the probability of a Type I error. If p < alpha,
    %               the difference is considered significant.
    %               Default: 0.05 (double).
    %   correctionType : Method for correcting for multiple comparisons.
    %               Rather than correct the significance criterion alpha, 
    %               the p-values are adjusted.
    %               Values:
    %                   'none'          : Do not correct the p-values.
    %                   'bonferroni'    : All p-values are divided by the
    %                                     total number of comparisons.
    %               Default: 'bonferroni'.
    % Outputs:
    %   pairStats   : Table object listing the indices of all data pairs,
    %                 measures type, and their test results. Fields:
    %                   'indA','indB'   : Data singleton array indices of  
    %                                     each item of the pair. Numeric
    %                                     singleton.
    %                   'subA','subB'   : Data subscript tuple indices of
    %                                     each item of the pair. Numeric
    %                                     array.
    %                   'isIndependent' : Measures type of the pair.
    %                                     Logical singleton.
    %                   'isSig','isSigCorr': Hypothesis test without/with
    %                                     correction for multiple 
    %                                     comparisons.
    %                   'p','pCorr'     : p-value without/with correction.
    %                   'r'             : Effect size.
    %                 Additional fields depend on the type of test
    %                 performed.
    %   normStats   : Table object listing the normality test results for
    %                 each data group.
    %   varStats    : Table object listing the homogeneity of variance test
    %                 results for each data pair.
    % Notes:
    % * Not 100% sure if homogeneity of variance was tested correctly.
    %   Variance was tested per data pair, but is should probably be tested
    %   globally across independent-measures variables per combination of
    %   repeated-measures variables. However, this is more complicated to
    %   implement, and may not result in a different result. In SPSS, it
    %   looks like each pair is tested separately, suggesting a pairwise
    %   approach might be ok:
    %     https://statistics.laerd.com/statistical-guides/independent-t-test-statistical-guide.php
    % * Testing of sphericity (repeated-measures) is not necessary, since
    %   comparing n=2 levels is guaranteed to yield a positive result.
    % History:
    % * 2020/09/__: Created, A.Berry.
    % * 2020/12/14: Re-written, A.Berry. Consistency and modularity
    %   improved.

    local                   = module();

    % Default options for name-value input arguments
    defaults.pairs          = {};
    defaults.isIndependent  = true;% (true/false) Is pair independent- or repeated-measures?
%     defaults.testAssumption = 'auto';% 'auto', 'parametric', 'nonparametric'
    defaults.normalityAssumption = 'auto';% 'auto', 'parametric', 'nonparametric'
    defaults.normTestFunc   = local.swtest;% Function handle
    defaults.varTestName    = 'LeveneAbsolute';% 'Bartlett', 'LeveneQuadratic', 'LeveneAbsolute', 'BrownForsythe', 'OBrien'
    defaults.alpha          = 0.05;
    defaults.correctionType = 'bonferroni';

    % Available string values for user options
    % * To sanitize the user selection to a specific spelling and case,
    %   regular expressions ('Expr') are used to identify matches with
    %   expected values ('Vals'). The user input is replaced with the
    %   matched value from 'Vals'.
    opts_normAssumptionVals = {'auto','parametric','nonparametric'};% Expected values
    opts_normAssumptionExpr = {'^auto','^par','^non-*par'};% Acceptable (case-insensitive) matches
    
    opts_correctionTypeVals = {'none','bonferroni'};
    opts_correctionTypeExpr = {'^none','^bonferroni'};

    
    %%% Pre-process user inputs
    % Overwrite default options with user inputs
    [opts,unusedArgs]       = parseOpts(defaults,varargin);
    
    % Argument 'data':
    % * Check container type and shape, and convert to a consistent format
    %   (cell array).
    % * Extra inputs can be categorical variables or subsets of the
    %   categories.
    [dataCell,dataLabels]   = data2cell(data,unusedArgs{:});
    szData                  = size(dataCell);
    if sum(szData > 1) > 2
        warning("Data grouped in more than 2 dimensions not yet tested. Proceed with caution.");
    end

    % Argument 'pairs': 
    % * Check type: 
    %   - Must be of type 'char' (keyword), numeric array, or cell array of
    %   numerics. 
    %   - If empty or keyword, populate with the associated numerics.
    % * Check size & shape: 
    %   - Must be 2D, with one of the dimensions of size 2. Note that
    %   'ismatrix' checks that ndims(pairs) == 2, but does not check
    %   whether these dimensions have a length of 0.
    %   - If the 2nd dimension does not have length 2, transpose it.
    if isempty(opts.pairs)
        pairInds    = permuteIndexPairs(szData,'all');
    elseif ischar(opts.pairs)
        % A keyword is entered to automatically generate a list of pairs.
        % Valid keywords: 'all','rows','columns'
        pairInds    = permuteIndexPairs(szData,opts.pairs);
    elseif isnumeric(opts.pairs)
        pairInds    = opts.pairs;
    elseif iscellnum(opts.pairs)
        % Indices input as subscript tuples. Convert to linear indices.
        % * To use 'sub2ind', each element (dimension) of the subscript 
        %   tuple must be unpacked as a separate argument. To do this,
        %   convert the tuple first to a cell array with 'num2cell'.
        cellSubs    = cellfun(@num2cell, opts.pairs, 'UniformOutput', false);
        pairInds    = cellfun(@(x) sub2ind(szData,x{:}), cellSubs);
    else
        error("Argument 'pairs' must be a numeric (cell) array or a valid keyword.");
    end
    if ~ismatrix(pairInds) || ~all(size(pairInds) > 0) || ~any(size(pairInds) == 2)
        error("Argument 'pairs' must have two columns and at least one row.");
    end
    if size(pairInds,2) ~= 2
        pairInds    = pairInds';
    end
    
    % Compute also the pair subscript tuples, since they are more human-readable
    pairSubs    = arrayfun(@(ind) ...
        cell2mat(dbl.getArg(1:numel(szData),'OutputCell',true,@ind2sub,szData,ind)), ...
        pairInds, 'UniformOutput', false);
    
    % Argument 'isIndependent':
    % * Check type: Must be logical (Boolean)
    % * Check size: Must be singleton or 1D array. Make it a column array.
    if ~islogical(opts.isIndependent)
        error("Argument 'isIndependent' must be of type 'logical' (true/false).");
    end
    if isscalar(opts.isIndependent)
        isIndependent   = repmat(opts.isIndependent, size(pairInds));
    elseif ismatrix(opts.isIndependent) && all(size(opts.isIndependent) > 0) && any(size(opts.isIndependent) == 1)
        isIndependent   = opts.isIndependent;
    else
        error("Argument 'isIndependent' must be either a singleton or a 1D array with as many elements as pairs.");
    end
    if size(isIndependent,2) ~= 1
        isIndependent   = isIndependent';
    end
      
    % Argument 'normalityAssumption':
    % * Check type: Must be of type 'char'
    % * Check values: Must match exactly one of the pre-defined regular
    %   expressions.
    if ~ischar(opts.normalityAssumption)
        error("Argument 'normalityAssumption' must be of type 'char'.");
    end
    isMatch     = matchExpr(opts.normalityAssumption, opts_normAssumptionExpr);
    if sum(isMatch) == 1
        normalityAssumption  = opts_normAssumptionVals{isMatch};
    else
        error("Argument 'normalityAssumption' must be one of the following: %s.",strjoin(strcat("'",opts_normAssumptionVals,"'"),', '));
    end
    
    % Argument 'normTestFunc':
    % * Must be a string or function handle.
    % * Further checking of handle validity is done elsewhere.
    if isa(opts.normTestFunc,'function_handle')
        normTestFunc    = opts.normTestFunc;
    elseif ischar(opts.normTestFunc)
        normTestFunc    = str2func(opts.normTestFunc);
    else
        error("Argument 'normTestFunc' must be of type 'function_handle' or 'char'.");
    end
    
    % Argument 'alpha':
    % * Must be a scalar numeric between 0 and 1 inclusive.
    if ~isscalar(opts.alpha) || ~isnumeric(opts.alpha) || ...
        (opts.alpha < 0) || (opts.alpha > 1)
        error("Argument 'alpha' must be a scalar numeric in the interval [0,1].");
    else
        alpha   = opts.alpha;
    end
    
    % Argument 'correctionType': 
    % * Must be of type 'char'
    % * Must match exactly one of the pre-defined regular expressions
    if ~ischar(opts.correctionType)
        error("Argument 'correctionType' must be of type 'char'.");
    end
    isMatch     = matchExpr(opts.correctionType,opts_correctionTypeExpr);
    if sum(isMatch) == 1
        correctionType  = opts_correctionTypeVals{isMatch};
    else
        error("Argument 'correctionType' must be one of the following: %s.",strjoin(strcat("'",opts_correctionTypeVals,"'"),', '));
    end
    
    
    %%% Perform the pairwise test
    nPairs  = size(pairInds,1);
    
    % Test normality of each distribution
    % * Determine whether any of the selected distributions are
    %   significantly different from the standard normal distribution.
    %   Note: Data not included in 'pairs' will be ignored.
    % * If 'pairTestType' has value 'auto', use the normality result to
    %   determine the test type.
    [isNormal,normStats] = normTest( dataCell,...
                                    'TestFunc',     normTestFunc,...
                                    'GroupInds',    unique(pairInds(:)),...
                                    'Alpha',        alpha );
    
    % Test homogeneity of variance of independent-measures variables
    % * Convert the cell-grouped data to a single stacked array with a 
    %   categorical variable denoting the group index.
    vTest     = [];
    for iPair   = nPairs:-1:1
        stackedData     = vertcat(dataCell{pairInds(iPair,1)}, dataCell{pairInds(iPair,2)});
        stackedInd      = [ repmat(pairInds(iPair,1),numel(dataCell{pairInds(iPair,1)}),1);
                            repmat(pairInds(iPair,2),numel(dataCell{pairInds(iPair,2)}),1) ];
        
        vTest(iPair).indA           = pairInds(iPair,1);
        vTest(iPair).indB           = pairInds(iPair,2);
        vTest(iPair).subA           = pairSubs{iPair,1};
        vTest(iPair).subB           = pairSubs{iPair,2};
        vTest(iPair).isIndependent  = isIndependent(iPair);
        if isIndependent(iPair)
            [vTest(iPair).p, temp]  = vartestn( stackedData, stackedInd, ...
                                                'TestType', opts.varTestName,...
                                                'Display',  'off' );
            vTest(iPair).f          = temp.fstat;
            vTest(iPair).df         = temp.df;
        else
            vTest(iPair).p          = 0;
        end
    end
    varStats                = struct2table(vTest);
    varStats.isHomogeneous  = (varStats.p > alpha);
    isHomogeneous           = all(varStats.isHomogeneous);
    
    % Optionally determine test assumptions automatically
    if strcmpi(normalityAssumption,'auto')
        if isNormal && isHomogeneous
            % None of the distributions are significantly non-normal and
            % none of the pairs have significantly non-homogeneous
            % variance.
            normalityAssumption    = 'parametric';
        else
            % One or more of the distributions is significantly non-normal
            % or one or more of the pairs has significantly heterogeneous
            % variance.
            normalityAssumption    = 'nonparametric';
        end
    end
    
    % Perform the pairwise tests
    % * 4 different tests are possible
    %       test                        'pairTestType'      'isIndependent'
    %      --------                     --------------      ---------------
    %   1) Independent-measures t-Test  'parametric'        true
    %   2) Repeated-measures t-Test     'parametric'        false
    %   3) Mann-Whitney u-Test          'nonparametric'     true
    %   4) Wilcoxon Signed-Rank Test    'nonparametric'     false
    if strcmpi(normalityAssumption,'parametric')
        pTest   = [];
        for iPair = nPairs:-1:1
            pTest(iPair).indA           = pairInds(iPair,1);
            pTest(iPair).indB           = pairInds(iPair,2);
            pTest(iPair).subA           = pairSubs{iPair,1};
            pTest(iPair).subB           = pairSubs{iPair,2};
            pTest(iPair).isIndependent  = isIndependent(iPair);
            if isIndependent(iPair)
                % Independent measures
                [pTest(iPair).isSig,pTest(iPair).p,ci,stats]  = ttest2(dataCell{pairInds(iPair,1)}, dataCell{pairInds(iPair,2)}, 'Alpha', alpha);
            else
                % Repeated measures
                [pTest(iPair).isSig,pTest(iPair).p,ci,stats]  = ttest(dataCell{pairInds(iPair,1)}, dataCell{pairInds(iPair,2)}, 'Alpha', alpha);
            end
            pTest(iPair).ci     = ci';
            for field = fieldnames(stats)'
                pTest(iPair).(field{:})     = stats.(field{:});
            end
            
            % Effect size
            pTest(iPair).effect_r       = sqrt( pTest(iPair).tstat^2/(pTest(iPair).tstat^2 + pTest(iPair).df) );
        end
        
    elseif strcmpi(normalityAssumption,'nonparametric')
        pTest   = [];
        for iPair = nPairs:-1:1
            pTest(iPair).indA           = pairInds(iPair,1);
            pTest(iPair).indB           = pairInds(iPair,2);
            pTest(iPair).subA           = pairSubs{iPair,1};
            pTest(iPair).subB           = pairSubs{iPair,2};
            pTest(iPair).isIndependent  = isIndependent(iPair);
            if isIndependent(iPair)
                % Independent measures
                [pTest(iPair).p, pTest(iPair).isSig, stats]  = ranksum(dataCell{pairInds(iPair,1)}, dataCell{pairInds(iPair,2)}, 'Alpha', alpha, 'Method', 'exact');
                [~,~,zStats]        = ranksum(dataCell{pairInds(iPair,1)}, dataCell{pairInds(iPair,2)}, 'Alpha', alpha, 'Method', 'approximate');
            else
                % Repeated measures
                [pTest(iPair).p, pTest(iPair).isSig, stats]  = signrank(dataCell{pairInds(iPair,1)}, dataCell{pairInds(iPair,2)}, 'Alpha', alpha, 'Method', 'exact');
                [~,~,zStats]        = signrank(dataCell{pairInds(iPair,1)}, dataCell{pairInds(iPair,2)}, 'Alpha', alpha, 'Method', 'approximate');
            end
            for field = fieldnames(stats)'
                % signed rank
                pTest(iPair).(field{:})     = stats.(field{:});
            end
            pTest(iPair).z      = zStats.zval;% z-value approximation
            
            % Effect size
            nObs    = numel(dataCell{pairInds(iPair,1)}) + numel(dataCell{pairInds(iPair,2)});
            pTest(iPair).effect_r  = pTest(iPair).z/sqrt(nObs);
        end
    else
        error("Unexpected value for internal variable 'pairTestType'.");
    end
    
    % Convert to a table object and append some metadata
    pairStats                           = struct2table(pTest);
    pairStats.Properties.Description    = ...
    sprintf('Pairwise test results for assumption ''%s'' and significance level alpha = %0.2f',normalityAssumption,alpha);
    pairStats.Properties.UserData    = struct(  'alpha',                alpha,...
                                                'normalityAssumption',  normalityAssumption );
    
    % Perform correction for multiple comparisons
    switch lower(correctionType)
        case 'none'
            % The corrected values are the same as the uncorrected values.
            pairStats.pCorr     = pairStats.p;
            pairStats.isSigCorr = pairStats.isSig;
        case 'bonferroni'
            % The p-value is corrected by multiplying by the number of
            % comparisons (pairs).
            pairStats.pCorr     = pairStats.p * nPairs;
            pairStats.isSigCorr = (pairStats.pCorr < alpha);
        otherwise
            error("Unexpected value for internal variable 'correctionType'.");
    end
    
    % Append variables from 'normStats' and 'varStats' tables into
    % 'pairStats' for easier readability.
    indA                        = arrayfun(@(x) find(x == normStats.dataInd), pairStats.indA);
    indB                        = arrayfun(@(x) find(x == normStats.dataInd), pairStats.indB);
    pairStats.isNormalA         = normStats.isNormal(indA);
    pairStats.pNormA            = normStats.p(indA);
    pairStats.isNormalB         = normStats.isNormal(indB);
    pairStats.pNormB            = normStats.p(indB);
    pairStats.isHomoVar         = varStats.isHomogeneous;
    pairStats.pVar              = varStats.p;
end

%% Local functions
function isMatch = matchExpr(str,exp)
    % Description:  Match one or more string options using regular
    %               expressions.
    isMatch     = ~cellfun(@isempty, regexp(str,exp,'ignorecase'));
end
function isType = iscellnum(data)
    % Description:  Check if variable is a cell array of numeric data.
    isType = ( iscell(data) && all(cellfun(@isnumeric,data(:))) );
end