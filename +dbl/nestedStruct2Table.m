function tabData = nestedStruct2Table(structData, varargin)
    % Description:  Recurse through fields of a multi-layered struct object
    %               and flatten the contents into a table object. 
    %               The purpose of this is to make it easier to view and 
    %               filter data sets based on different categorical 
    %               variables.
    % Inputs:
    %   structData  : The hierarchical struct object to flatten.
    % Optional name-value inputs:
    %   isUniform   :   Whether or not the branches are expected to have 
    %                   the same lengths. If the branches have different 
    %                   lengths, the field names cannot be vertically 
    %                   concatenated in the table, so the names are placed 
    %                   in a singleton cell.
    %                       logical: true, false (default)
    %   arrayLabelFmt : The formatting rule for printing struct array
    %                   labels, combining a string '%s' and an integer
    %                   '%d'.
    %                       string: '%s(%d)' (default)
    %   parentFields :  Initial contents of the list of fields in each
    %                   branch.
    %                       cellstr array: {} (default)
    %   tabData     :   Initial contents of the table of field-data values.
    %                       table: [] (default)
    % Outputs:
    %   tabData     :   Table object with as many rows as branches in the
    %                   (multi-level) struct object. The columns are:
    %                   parentFields: Cell array of sequential struct field
    %                                 names for that branch.
    %                   data        : The data stored at the end of the
    %                                 branch (any data type other than
    %                                 struct).
    % Examples:
    %   * Example 1: Singleton struct
    %       >> a.b1.c1 = 1;
    %       >> a.b1.c2 = 2;
    %       >> a.b2.c1 = 3;
    %       >> a.b2.c2 = 4;
    %       >> tabData = nestedStruct2Table(a,'isUniform',true);
    %         parentFields       data
    %       _________________    ____
    %       {'b1' }    {'c1'}     1  
    %       {'b1' }    {'c2'}     2  
    %       {'b2' }    {'c1'}     3  
    %       {'b2' }    {'c2'}     4    
    %   * Example 2: Array struct
    %       >> a.b(1).c1 = 1;
    %       >> a.b(1).c2 = 2;
    %       >> a.b(2).c1 = 3;
    %       >> a.b(2).c2 = 4;
    %       >> tabData = nestedStruct2Table(a,'isUniform',true);
    %         parentFields       data
    %       _________________    ____
    %       {'b(1)'}   {'c1'}     1  
    %       {'b(1)'}   {'c2'}     2  
    %       {'b(2)'}   {'c1'}     3  
    %       {'b(2)'}   {'c2'}     4  
    % History:
    % * 2020/10/14: Created, A.Berry.
    
    local               = module();

    % Default options
    opts.isUniform      = false;
    opts.arrayLabelFmt  = '%s(%d)';
    opts.parentFields   = {};
    opts.tabData        = [];
    
    % Load any (partial) options structure and overwrite defaults
    isStruct    = cellfun(@isstruct, varargin, 'UniformOutput',true);
    if any(isStruct)
        optsUser    = varargin{find(isStruct,1,'first')};
        for optField = fieldnames(optsUser)'
            opts.(optField{:})  = optsUser.(optField{:});
        end
    end
    % Overwrite with user-supplied name-value options
    for iOpt = fieldnames(opts)'
        if any(strcmpi(varargin, iOpt))
            opts.(iOpt{:})      = varargin{find(strcmpi(varargin,iOpt{:}),1,'first') + 1};
        end
    end
    
    
    % Unpack recursion variables
    parentFields    = opts.parentFields;
    tabData         = opts.tabData;

    if isstruct(structData)
        % Recurse until we are at the tip of the branch
        for field = fieldnames(structData)'
            nextChild   = structData.(field{:});
            if isstruct(nextChild) && (numel(nextChild) > 1)
                % In the case that the next child is an array of structs,
                % add a numeric suffix to the field name.
                for iArray = 1:numel(nextChild)
                    childField  = sprintf(opts.arrayLabelFmt, field{:}, iArray);
                    tabData     = local.nestedStruct2Table(nextChild(iArray), 'parentFields', [parentFields,childField], 'tabData', tabData, opts);
                end
            else
                % Don't bother with a suffix if the child is a singleton.
                tabData     = local.nestedStruct2Table(nextChild, 'parentFields', [parentFields,field], 'tabData', tabData, opts);
            end
        end
    else
        % At branch tip: add another row to the table
        if ~opts.isUniform
            parentFields    = {parentFields};% Place in a singleton cell to avoid issues concatenating dissimilar cell arrays
            structData      = {structData};% 2021/01/21
        end
        tabData         = [ tabData; ...
                            table(parentFields, structData, 'VariableNames', {'parentFields','data'}) ];
    end
end