function levels = uniqueN(data,indexVars,order)
    % Description:  For data of various types, find all unique values in
    %               each specified column or field. 
    %               This can be used, for example, to generate a list of
    %               levels of categorical values prior to performing
    %               statistical comparisons.
    % Inputs:
    %   data:       Table, struct, cell, or numeric array containing
    %               columns or fields of 1D categorical variables.
    %   indexVars:  Indices of columns or names of the fields to list.
    %   order:      (Optional) The order in which the categorical variables
    %               are listed. Options: 'stable' (default), 'sorted'.
    % Outputs:
    %   levels:     Cell array of the unique elements.
    % Note:
    % * The 'unique' function by default sorts the list of levels
    %   alpha-numerically.
    
    % Default parameters
    if ~exist('order','var') || isempty(order)
        order   = 'stable';
    end
    
    % Parse inputs
    if ischar(indexVars) || isstring(indexVars)
        indexVars    = {indexVars};
    end
    
    nVars       = numel(indexVars);
    levels      = cell(size(indexVars));
    if isnumeric(indexVars) && (isnumeric(data) || iscell(data))
        for iVar = 1:nVars
            levels{iVar}     = unique(data(:,indexVars(iVar)),order);
        end
    elseif isnumeric(indexVars) && istable(data)
        for iVar = 1:nVars
            levels{iVar}     = unique([data{:,indexVars(iVar)}],order);
        end
    elseif iscell(indexVars) && (istable(data) || isstruct(data))
        for iVar = 1:nVars
            levels{iVar}     = unique(data.(indexVars{iVar}),order);
        end
    else
        error('Unsupported data or indexing variable type.');
    end
end