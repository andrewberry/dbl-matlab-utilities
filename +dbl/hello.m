function message = hello()
    % Description:  A simple function to demonstrate working with a
    %               package.
    message     = 'Hello, world!';
end