function effect = computeEffect(data,n,effectType)
    % Description:  Compute an effect size quantifying the magnitude of
    %               differences between conditions.
    %               Note that these are approximate, since the expression
    %               in general depends on the number of variables and study
    %               design.
    % Notes:
    % * No supporting reference can be found for the calculation of
    %   Field2002, so seems fishy.
    % * Wikipedia article (https://en.wikipedia.org/wiki/Effect_size)
    %   references an article:
    %   Olejnik2003, "Generalized Eta and Omega Squared Statistics: 
    %   Measures of Effect Size for Some Common Research Designs"
    %   (available on ResearchGate), but the computations are too complex
    %   to generalize without further information.
    % * Interpretation:
    %   - Field states that effect sizes for ANOVAs aren't very meaningful
    %   when more than 2 groups are being compared: 
    %   https://www.discoveringstatistics.com/repository/effectsizes.pdf
    % NOTE:
    % * The number of observations may have to be calculated differently
    %   for independent-measures ANOVA.

    if ~exist('effectType','var') || isempty(effectType)
        effectType  = {'eta','omega1','omega2'};
    end

    if istable(data)
        if ~isempty(data.Properties.RowNames)% 'ranova' output
            % Row names indicate the independent variable and associated errors
            % Effect sizes must use the appropriate error in the calculations.
            % Divide the rows into blocks accordingly.
            rowNames    = data.Properties.RowNames;
        elseif any(strcmp(data.Properties.VariableNames,'Between'))% 'anova' output
            % No row names, but column called 'between' with values
            % 'constant',<imVar>,'Error'
            rowNames    = data.Between;
        else
            error('Data row contents could not be identified. Format not supported.');
        end
    else
        error('Only table data supported.');
    end

%     effect          = table('Size',[size(data,1),numel(effectType)],'VariableNames',effectType,'VariableTypes',repmat({'internal.stats.DoubleTableColumn'},size(effectType)));
    effect          = table('Size',[size(data,1),numel(effectType)],'VariableNames',effectType,'VariableTypes',repmat({'double'},size(effectType)));

    isErrorRow      = ~cellfun(@isempty,regexp(rowNames,'^Error'));
    indErrorRow     = find(isErrorRow);

    % Compute values for each block separately
    blockStart  = 1;
    for iBlock = 1:length(indErrorRow)
        indsData    = blockStart:(indErrorRow(iBlock)-1);
        dataBlock   = data(indsData,:);            
        errorBlock  = data(indErrorRow(iBlock),:);

        % Compute the different effects
        SSm         = dataBlock.SumSq;
        SSr         = errorBlock.SumSq;
        SSt         = SSm + SSr;
        MSm         = dataBlock.MeanSq;
        MSr         = errorBlock.MeanSq;
        dfm         = dataBlock.DF;
        dfr         = errorBlock.DF;
        dft         = dfm + dfr;
        
        n           = dft;

        effect.eta(indsData)       = sqrt( SSm./SSt );% Field2002 p181
        effect.omega1(indsData)    = sqrt( (MSm - MSr)./(MSm + (n-1).*MSr) );% Field2002 p181
        effect.omega2(indsData)    = sqrt( (SSm - dfm*MSr)./(SSt + MSr) );% https://en.wikipedia.org/wiki/Effect_size

%         if ~isreal(effect.omega1(indsData)) || ~isreal(effect.omega2(indsData))
%             1;
%         end
        
        blockStart  = indErrorRow(iBlock) + 1;
    end

    % Convert to special data type to hide empty rows in table
    % * Strangely, this only works with name indexing and not with column
    %   indexing.
    for field = effect.Properties.VariableNames
        effect.(field{:}) = internal.stats.DoubleTableColumn(effect.(field{:}), isErrorRow);
    end

end

%% Local functions
function outval = zeroIfComplex(inval)
    % Omega^2 values can be negative, and the resultant omega values will
    % be complex. 
    % This article says that negative omega^2 should be reported as-is,
    % although interpreted as zero:
    % https://link.springer.com/article/10.3758/s13428-016-0760-y
    if isreal(inval)
        outval  = inval;
    else
        outval  = zeros(size(inval));
    end
end