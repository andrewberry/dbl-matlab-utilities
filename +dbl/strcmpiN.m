function isMatch = strcmpiN(s1,s2)
    % Description:  Make strcmpi accept 1D array arguments for both
    %               dimensions.
    % Inputs:
    %   s1 :    (mx1) or (1xm) cell array of strings
    %   s2 :    (1xn) or (nx1) cell array of strings
    % Outputs:
    %   isMatch     :   (mxn) or (nxm) logical array
    
    % Reshape the 2nd argument to make sure output is the same shape as s1
    [nx,ny]     = size(s1);
    if nx > ny% s1 is a column, force s2 to be a row
        s2      = reshape(s2,1,[]);
    else% s1 is a row, force s2 to be a column
        s2      = reshape(s2,[],1);
    end
    
    isMatch     = cell2mat(arrayfun(@(x) strcmpi(s1,x), s2, 'UniformOutput', false));
end