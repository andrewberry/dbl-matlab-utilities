# Read me
## Repository contents
This repository contains publicly-accessible Matlab scripts for plotting and analyzing biomechanics data. Feedback and contribution to this repository is encouraged.

Contents:
* `+dbl`: Matlab package with member functions for performing stastical analyses and creating box plots with significance brackets.

<!-- ## Cloning this repository with Git
This repository contains submodules (nested Git repositories), which requires special commands to retrieve the nested information:
* Clone: `git clone --recursive <projectURL> <optionalNewFolderName>`
* Update missing submodule information: `git submodule update --init --recursive` -->

## Git submodules
In Git, it is possible to embed Git projects (repositories) within one another. For example, you might have Git repositiories for different research projects you are working on, and a single Git repository containing all your favourite Matlab scripts that are reused in each project, and possibly updated or expanded with each re-use. In principle, you could copy-paste these scripts into each research project, but it is difficult to keep track of changes you make to these scripts for later re-use. 
One solution could be to add relative paths in each of your projects to point to a single location containing the scripts. However, this is cumbersome if you have collaborators that must be given access to all necessary repositories and need to replicate your directory structure on their own computer. 
A more flexible solution is to embed the scripts repository in all your research project respositories, thereby maintaining a common record for all changes and imposing fewer constraints on your collaborators.

In Git terminology, a repository that is embedded within another is called a `submodule`. You create it as you would create any other Git project -- i.e., by making a new project on a hosting platform (e.g., Gitlab, Github, Bitbucket, or even Overleaf) and pushing content to it. Thus far, it is just a normal Git repository that is not embedded in another project, and you are free to clone (download) it as you would any other repository.

Instructions for creating and accessing submodules:

1. Add a submodule to a cloned repository:
    * You must already have a local copy of the parent repository on your computer (ignore this if it is already downloaded): `git clone <url> <optionalNewFolderName>`. 
    * In the terminal, navigate (`cd`) to where you would like to place the submodule. When added, it will appear as a new folder within the parent repository.
    * Add the submodule: `git submodule add <url> <newFolderName>`
    * Download the contents of the submodule(s) (newer versions of Git should do this automatically): `git submodule update --init --recursive`
        - Explanation: `init` updates the Git metadata file to recognize the presence of submodules, `update` downloads content in the submodules, and `recursive` does this for all nested nested submodules. Similar to a repeated application of the commands `git submodule init` and `git submodule update`.
    * Example:
    ```bash
    cd ~/GitProjects
    git clone https://gitlab.tudelft.nl/andrewberry/researchProject1.git myFirstResearchProject
    cd myFirstResearchProject
    git submodule update --init --recursive
    ```

2. Cloning a project already containing submodules: 
    * Using the usual `git clone` command, submodules are not downloaded by default (they will appear as empty folders).
    * Clone (download) the project with all submodules: `git clone --recursive <projectURL> <optionalNewFolderName>`

3. Download submodules in a previously-cloned project
    * Downloading repository updates as normal will show empty folders where the submodules will be: `git pull`
    * Download the contents of the submodule(s): `git submodule update --init --recursive`

In general, if you open a submodule directory in the terminal, you interact directly with the repository that the submodule links to, rather than the parent repository. This means that you can look at the Git log, make commits, push and pull to the nested repository as if you had cloned it separately. However, at the parent directory, these actions will instead be applied to the parent repository.