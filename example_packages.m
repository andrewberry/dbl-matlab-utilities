%% Package 'dbl'
% This is a package
% * Packages are collections of functions that keep a local name-space.
%   This means that function names can be re-used without the threat of
%   conflict with similarly-named functions or variables. One difference
%   between this and a class is that using a package doesn't involve
%   creating an instance of an object: it is simply a collection of
%   definitions (functions, variables, etc).
% * A package in Matlab exists as a folder prefixed by '+'. It is
%   automatically recognized as a special data structure.
% * Instructions for how to add and use a package are listed below.
%
% Including this package in a project
% * There are multiple possibilities:
%   1) Copy-paste these files into your project(s). This is not 
%       recommended, because it can be difficult to keep track of any 
%       changes made to the package.
%   2) Place these files external to your project(s) in a single location, 
%       and add local path variables to point reference this location from
%       each project. This may be problematic if the project is
%       version-controlled and you have collaborators, as it may oblige
%       them to use the same local directory structure as you or requires
%       documenting procedures on how to set up individualized path
%       variables.
%   3) If the project is on Git, include this package as a submodule (a Git
%       repository nested within another Git repository). Refer to the
%       ReadMe on how to do this. The advantage of this is that no
%       individualized setup is required for collaborators, and changes to
%       the package can be saved for future reuse. The disadvantage is that
%       a local copy of the package exists in each project directory;
%       however this is not usually a problem due to the small size of the
%       package contents.           
%
% Adding this package to the Matlab path
% * Add just the root directory where the packages (folders prefixed with 
%   '+') are found. Do not try to add the contents of the packages.
% * 'addpath(<path>)': Insert the relative (recommended) or absolute 
%   location of this  folder as a string. E.g., your project contains a 
%   folder 'utilities' containing all generic scripts including this 
%   package, then:
%       >> addpath('utilities/dblLib');
% * Note that directories within a package that are not themselves
%   (sub)packages are not accessible. They are private to the package.
%
% Querying the contents of a package
% * You can either open the package folder or invoke one of the following
%   commands
%       >> help('dbl')  % Produces a list of all contents
%       >> what('dbl')  % Produces a list of accessible definitions and their types
disp('Package definitions:');
contents    = what('dbl');
disp(contents);

% Calling functions from the package
% * Again, there are multiple options
%   1) Use the default package name-space. Prefix all function calls with
%   the package name, e.g.:
disp('Direct referencing with original namespace:');
disp(dbl.hello());

%   2) Create your own alias name-space. You might do this for readability
%   or shortening the length of commands (especially if their are packages
%   nested in multiple layers, e.g.
%       'mypack1.mysubpack.mysubsubpack.command()'
%   Note, however, that the original namespace remains valid syntax. This
%   simply creates a handle to the same command.
disp('Direct referencing with modified namespace:');
myName  = importAs('dbl');
disp(myName.hello());

%   3) Expand the contents of the package. This removes the need to prefix
%   commands with the namespace. However, it potentially also creates
%   conflicts with similarly-named functions or variables already on the
%   current Matlab path.
%   The import list is local and is cleared at the end of execution; to
%   explicitly clear it:
%       >> clear import
disp('Importing:');
import dbl.hello
disp(hello());

disp('Contents of import list:');
L = import;
disp(L);